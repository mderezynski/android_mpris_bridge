/* vim: ts=4 sw=4
 */
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;

const St = imports.gi.St;
const Main = imports.ui.main;
const Mainloop = imports.mainloop;
const Shell = imports.gi.Shell;
const Atk = imports.gi.Atk;

const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Lang = imports.lang;
const FileUtils = imports.misc.fileUtils;
const Util = imports.misc.util;
const Clutter = imports.gi.Clutter;

const IndicatorName = 'Mpris AP';

const ApProxy = Gio.DBusProxy.makeProxyWrapper("<node><interface name='org.mpris.AccessPoint1'><method name='Status'><arg name='value' type='a{sv}' direction='out'/></method></interface></node>");
const FdoBusProxy = Gio.DBusProxy.makeProxyWrapper("<node><interface name='org.freedesktop.DBus'><method name='ListNames'><arg name='value' type='ss' direction='out'/></method><signal name='NameOwnerChanged'><arg type='s'/><arg type='s'/><arg type='s'/></signal></interface></node>");

let label3;
let label2;
let ap;
let icon;

/**
 * Gicon Menu Item Object
 */
function PopupGiconMenuItem() {
    this._init.apply(this, arguments);
}

PopupGiconMenuItem.prototype = {
    __proto__: PopupMenu.PopupBaseMenuItem.prototype,

    _init: function (text, params) {
        PopupMenu.PopupBaseMenuItem.prototype._init.call(this, params);

        this.label = new St.Label({ text: text });
        this._icon = new St.Icon({ icon_name: 'system-run-symbolic',
                             style_class: 'system-status-icon' });
        this.actor.add_child(this._icon, { align: St.Align.END });
        this.actor.add_child(this.label);
    },
};

/**
 * QuickLaunch Object
 */
const QuickLaunch = new Lang.Class({
    Name: IndicatorName,
    Extends: PanelMenu.Button,

    _updateStatus: function() {
    
        let du = ap.StatusSync();
        let apName = du[0].name.get_string()[0];
        let apPort = du[0].port.get_int32();
        
        label2.text = apName;        
        label3.text = "Running on port " + apPort.toString();       

    },

    _init: function(metadata, params) {

        this.parent(null, IndicatorName);
        this.actor.accessible_role = Atk.Role.TOGGLE_BUTTON;
        this.connect('destroy', Lang.bind(this, this._onDestroy));

        ap = new ApProxy(Gio.DBus.session, "org.mpris.AccessPoint1.ReferenceImplementation", "/MprisAP");
        dbus = new FdoBusProxy(Gio.DBus.session, "org.freedesktop.DBus", "/org/freedesktop/DBus");
        
        icon = new St.Icon({ style_class: 'system-status-icon', icon_name: 'gtk-yes' })
        this._label = new St.Label({ style_class: 'panel-label', text: _("Mpris AP") });

        let mainBox = new St.BoxLayout({vertical: false, style_class: 'back'});
        mainBox.add_actor(icon);
        mainBox.add_actor(this._label);
        this.actor.add_actor(mainBox);
        
        let section = new PopupMenu.PopupMenuSection("Mpris AP Connection");
     
        label2 = new St.Label({ style_class: 'mprisap-label', x_expand: true, text: "(not connected)", x_align: Clutter.ActorAlign.CENTER});
        label3 = new St.Label({ style_class: 'mprisap-label-small', x_expand: true, text: "", x_align: Clutter.ActorAlign.CENTER });

        section.actor.add_actor(label2);
        section.actor.add_actor(label3);

        names = dbus.ListNamesSync()[0]
        
        if(names.indexOf("org.mpris.AccessPoint1.ReferenceImplementation") > -1) {
        
            this._updateStatus();
        
        } else {

            label2.text = "AP Not Running";
            label2.style_class = 'mprisap-label-insensitive';
            label3.text = "";
            
            icon.icon_name = 'gtk-no';

        }
        
        dbus.connectSignal("NameOwnerChanged",Lang.bind(this,function (proxy,sender,[bus,old_owner,new_owner]) {
	        if (/^org\.mpris\.AccessPoint1\.ReferenceImplementation/.test(bus)) {
	            
	            
	            if (!old_owner && new_owner) {
	            
	                    let du = ap.StatusSync();
                        let apName = du[0].name.get_string()[0];
                        let apPort = du[0].port.get_int32();
                        
                        label2.text = apName;   
                        label2.style_class = 'mprisap-label';                             
                        label3.text = "Running on port " + apPort.toString();       

                        icon.icon_name = 'gtk-yes';

	            } else if (old_owner && !new_owner) {

                        label2.text = "AP Not Running";
                        label2.style_class = 'mprisap-label-insensitive';
        	            label3.text = "";
        	            
        	            icon.icon_name = 'gtk-no';
                    
	            }
	        }
	    }));
        
        this.menu.addMenuItem(section);

    },

    _onDestroy: function() {
    },

    /**
     * create popoup menu item with callback
     */
    _createMenuItem: function(name, callback) {
        let menuItem = new PopupGiconMenuItem(name, {});
        menuItem.connect('activate', Lang.bind(this, function (menuItem, event) {
                    callback(menuItem, event);
        }));

        return menuItem;
    },

});


/**
 * Extension Setup
 */
function init() {
}

let _indicator;

function enable() {
    _indicator = new QuickLaunch();
    Main.panel.addToStatusArea(IndicatorName, _indicator);
}

function disable() {
    _indicator.destroy();
    _indicator = null;
}
