#!/usr/bin/env python

import dbus
import dbus.service
from dbus.mainloop.glib import DBusGMainLoop
import glib
import gobject
import logging
from mprisap.ap import MprisAP 

class Launcher():

	def do(self):

		logger = logging.getLogger("mprisap")
		handler = logging.FileHandler("/tmp/mprisap.log")
		formatter = logging.Formatter("%(asctime)s %(levelname)s %(message)s")
		handler.setFormatter(formatter)
		logger.addHandler(handler)
		logger.setLevel(logging.INFO)

		DBusGMainLoop(set_as_default=True)

		logger.info('Initializing MprisAP')

		bus = dbus.StarterBus()

		if bus is None:
			bus = dbus.SessionBus()

		loop = gobject.MainLoop()

		name = dbus.service.BusName("org.mpris.AccessPoint1.ReferenceImplementation", bus)
		object = MprisAP(bus, '/MprisAP', loop)

		logger.info('Starting Glib mainloop')

		loop.run()

l = Launcher()
l.do()
