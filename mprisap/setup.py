#!/usr/bin/env python

from distutils.core import setup

setup(name='mprisap-reference',
      version='0.0.5',
      description='The upstream reference implementation of an MPRIS Access Point',
      author='Milosz Derezynski',
      author_email='mdereznski@gmail.com',
      url='https://bitbucket.org/mderezynski/android_mpris_bridge',
      scripts=['mprisap-launch','mprisap-shutdown','mprisap-status'],
      packages=['mprisap','mprisap.launch'],
      data_files=[('share/dbus-1/services', ['org.mpris.AccessPoint1.ReferenceImplementation.service'])]
     )
