import dbus
import dbus.service
from dbus.mainloop.glib import DBusGMainLoop
import glib
import gobject
from glibjsonrcpserver import GlibJSONRPCServer
import jsonrpclib
import ConfigParser
from gtk import icon_theme_get_default, ICON_LOOKUP_USE_BUILTIN
import base64
import cStringIO as StringIO
import avahi
import urllib2
from encodings.idna import ToASCII
import traceback
from collections import OrderedDict
import os
import getpass
from subprocess import call

class PaCtrl():

	def __init__(self, target_bus, ap):

		self.ap = ap
		self.bus = target_bus

		call(["pactl","load-module","module-dbus-protocol"])

		self.conn = self.connect()
		self.core = self.conn.get_object(object_path='/org/pulseaudio/core1')
		self.conn.call_on_disconnection(self.on_disconnect)
		
		self.conn.add_signal_receiver(self.on_port_change, 'ActivePortUpdated')
		self.core.ListenForSignal('org.PulseAudio.Core1.Device.ActivePortUpdated', dbus.Array(signature='o'))
			
		print('Connected to pulseaudio: enabling headset plug control')
	
	def get_bus_address(self):

		return self.bus\
			.get_object(
				'org.PulseAudio1', 
				'/org/pulseaudio/server_lookup1'
			).Get(
				'org.PulseAudio.ServerLookup1',
				'Address',
				dbus_interface=dbus.PROPERTIES_IFACE
			)

	def connect(self):
		srv_addr = self.get_bus_address()
		return dbus.connection.Connection(srv_addr)
	
	def on_disconnect(self,con):
		print ('disconnected from pulseaudio, try to reconnect...')
		
	def on_port_change(self,port_addr):

		sink = self.conn.get_object(object_path=port_addr)
		helper = DBUSHelper(self.conn)
		props = helper.get_properties("org.PulseAudio.Core1", port_addr)	
		desc = props.Get("org.PulseAudio.Core1.DevicePort", "Name")

		print desc

		if desc == "analog-output-speaker":
			self.ap.freeze()
			self.ap.pauseLocal()
			self.ap.thaw()

		elif desc == "analog-output-headphones":
			self.ap.resumeLastPlaying()

	def getName(self, obj, itf):
		return obj.Get('org.PulseAudio.Core1.{}'.format(itf),
					'Name', dbus_interface=dbus.PROPERTIES_IFACE)

class MprisAP(dbus.service.Object):

    def __init__(self, bus, path, loop):
	self.loop = loop
        dbus.service.Object.__init__(self, bus, path)
        self.ap = MprisAccessPoint(bus)

    @dbus.service.method("org.mpris.AccessPoint1",
			 in_signature='', out_signature='a{sv}')
    def Status(self):

	status = {}

	status['name'] = self.ap.getAvahi().getName()
	status['port'] = self.ap.getRpc().getPort()
	status['mdns-status'] = self.ap.getAvahi().getState()

	origin_list = []

	for server in self.ap.getMonitors():
		for monitor in self.ap.getMonitors()[server]:
			origin_list.append(monitor.getOrigin())

	uniq_clients = list(OrderedDict.fromkeys(origin_list)) 

	if len(uniq_clients) > 0:
		status['clients'] = uniq_clients

	afPeers = self.ap.getAfPeers()

	if len(afPeers) > 0:
		status['audiofocus_peers'] = dbus.Array(afPeers)

	if self.ap.pactrl is not None:
		status['headset_plug_control'] = 'enabled'
	else:
		status['headset_plug_control'] = 'unavailable'

	return status	

    @dbus.service.method("org.mpris.AccessPoint1",
                         in_signature='', out_signature='')
    def Exit(self):
        self.ap.shutdown()
	self.loop.quit()

class DBUSHelper:

	def __init__(self,target_bus):
	
		self.bus = target_bus

	def get_properties( self, server, dbus_obj_path ):

		dbus_object = self.bus.get_object(server,dbus_obj_path)
		properties_manager = dbus.Interface(dbus_object, 'org.freedesktop.DBus.Properties')   
	 
		return properties_manager

class MprisAccessPointAvahiHelper:

	def __init__(self,system_bus, port):

		self.bus = system_bus
		self.serviceType = "_mprisap._tcp"
		self.servicePort = port 
		self.serviceTXT = "mprisap"
		self.domain = "local"
		self.host = ""
		self.group = None
		self.rename_count = 12
	
		self.groups = {}

		self.busHelper = DBUSHelper(system_bus)

		self.avahiSrv = dbus.Interface(self.bus.get_object( avahi.DBUS_NAME, avahi.DBUS_PATH_SERVER ), avahi.DBUS_INTERFACE_SERVER )
		self.__server_state_changed( self.avahiSrv.GetState() )
		self.avahiSrv.connect_to_signal( "StateChanged", self.__server_state_changed )

	def getState(self):
		return self.group.GetState()

	def getName(self):
		return self.serviceName

	def shutdown(self):

		self.__remove_service()

	def __exit__(self, type, value, traceback):

		self.__remove_service()

	def __get_host_identity(self):

		props = self.busHelper.get_properties('org.freedesktop.hostname1', '/org/freedesktop/hostname1')
		return str(props.Get('org.freedesktop.hostname1','Hostname'))

	def encode_dns(self, name):
		out = []
		for part in name.split('.'):
			if len(part) == 0: continue
			out.append(ToASCII(part))
		return '.'.join(out)

	def createRR(self, name):
		out = []
		for part in name.split('.'):
			if len(part) == 0: continue
			out.append(chr(len(part)))
			out.append(ToASCII(part))
		out.append('\0')
		return ''.join(out)	

	def add_service(self):

		self.avahi_group = self.avahiSrv.EntryGroupNew()

		self.group = dbus.Interface(
				self.bus.get_object( avahi.DBUS_NAME, self.avahi_group),
				avahi.DBUS_INTERFACE_ENTRY_GROUP)
		self.group.connect_to_signal('StateChanged', self.__entry_group_state_changed)


		self.serviceName = self.__get_host_identity()+"-"+str(getpass.getuser())

		print "Adding service '%s' of type '%s' ..." % (self.serviceName, self.serviceType)

		self.group.AddService(
				avahi.IF_UNSPEC,    #interface
				avahi.PROTO_UNSPEC, #protocol
				avahi.PUBLISH_UPDATE, #flags
				self.serviceName, self.serviceType,
				self.domain, self.host,
				dbus.UInt16(self.servicePort),
				avahi.string_array_to_txt_array(self.serviceTXT))

		self.group.Commit()

	def __remove_service(self):

		if not self.group is None:
			self.group.Reset()
			self.group = None

	def __server_state_changed(self,state):

		if state == avahi.SERVER_COLLISION:
			print "WARNING: Server name collision"
			self.__remove_service()
		elif state == avahi.SERVER_RUNNING:
			self.add_service()

	def __entry_group_state_changed(self,state, error):

		print "state change: %i" % state

		if state == avahi.ENTRY_GROUP_ESTABLISHED:
			print "Service established."
		elif state == avahi.ENTRY_GROUP_COLLISION:

			self.rename_count = self.rename_count - 1
			if self.rename_count > 0:
				self.serviceName = server.GetAlternativeServiceName(name)
				print "WARNING: Service name collision, changing name to '%s' ..." % name
				self.__remove_service()
				self.add_service()

			else:
				print "ERROR: No suitable service name found after %i retries, exiting." % n_rename
				main_loop.quit()

		elif state == avahi.ENTRY_GROUP_FAILURE:
			print "Error in group state changed", error
			main_loop.quit()
			return

class MprisBaseInterfaceProxy:

	def __init__(self, server, target_bus, mpris_iface):

		self.server = server
		self.bus = target_bus
		self.dbus_helper = DBUSHelper(self.bus)

		if mpris_iface is not None:
			self.iface = "org.mpris.MediaPlayer2."+mpris_iface
		else:
			self.iface = "org.mpris.MediaPlayer2";

	@property
	def mpris_iface( self ):

		dbus_object = self.bus.get_object(self.server,'/org/mpris/MediaPlayer2')
		iface = dbus.Interface(dbus_object, self.iface)

		return iface

	def setprop( self, prop, value ):

		props = self.dbus_helper.get_properties(self.server, '/org/mpris/MediaPlayer2')
		props.Set(self.iface, prop, value) 

	def getprop( self, prop ):

		props = self.dbus_helper.get_properties(self.server, '/org/mpris/MediaPlayer2')
		return props.Get(self.iface, prop) 

class MprisMprisProxy(MprisBaseInterfaceProxy):

	def __init__(self, server, target_bus):
		MprisBaseInterfaceProxy.__init__(self, server, target_bus, None)


	def Raise(self):
		self.mpris_iface.Raise()		

	@property
	def identity(self):
		return str(self.getprop("Identity"))

	@property
	def icon(self):
		desktopFile = str(self.getprop("DesktopEntry"))
		fullPath = "/usr/share/applications/"+desktopFile+".desktop"
		config = ConfigParser.ConfigParser()
		config.read(fullPath)
		icon = config.get("Desktop Entry","Icon")
		icon_file = self.__load_icon(128, icon)
		io = StringIO.StringIO()
		icon_file.save_to_callback(io.write, 'png')
		buffer = io.getvalue()
		return base64.b64encode(buffer) 

	def __load_icon(self, size, *alternatives):

		    it = icon_theme_get_default()
		    alternatives = list(alternatives)
		    name = alternatives.pop(0)

		    try:
			return it.load_icon(name, size, ICON_LOOKUP_USE_BUILTIN)
		    except:
			if alternatives:
			    return load_icon(size, *alternatives)



class MprisPlayerProxy(MprisBaseInterfaceProxy):

	def __init__(self, server, target_bus):
		MprisBaseInterfaceProxy.__init__(self, server, target_bus, "Player")
		self.method_mapping = { 'playPause':self.play_pause,'next':self.go_next,'previous':self.go_previous,'volume':self.volume,'metadata':self.metadata, 'playbackstatus':self.playbackstatus }
	
	def __getattr__(self,name):
		if name in self.method_mapping:
			return self.method_mapping[name]	
		else:
			return AttributeError 

	def volume(self, d):
		self.setprop("Volume",d)

	@property
	def metadata(self):
		return self.getprop("Metadata")	

	@property
	def playbackstatus(self):
		return self.getprop("PlaybackStatus")

	def play_pause(self):
		if self.playbackstatus != "Playing":
			obj = MprisMprisProxy(self.server, self.bus)
			obj.Raise()
		self.mpris_iface.PlayPause()

	def go_next(self):
		self.mpris_iface.Next()
		
	def go_previous(self):
		self.mpris_iface.Previous()

	def pause_always(self):

		if self.playbackstatus == "Playing":
			self.play_pause()


class MprisTrackListProxy(MprisBaseInterfaceProxy):

	def __init__(self, server, target_bus):
		MprisBaseInterfaceProxy.__init__(self, server, target_bus, "TrackList")

	@property
	def tracklist(self):

		try:
			tracks = self.getprop("Tracks")
			tracks_meta = self.mpris_iface.GetTracksMetadata(tracks) 

			meta_list = []

			for track in tracks_meta:
				meta_list.append(str(track['xesam:title']))

			return meta_list
		except:
			return None 

	def goToTrack(self, n):

		try:
			tracks = self.getprop("Tracks")
			self.mpris_iface.GoTo(tracks[n])

		except:
			pass

		

class MprisRpcDispatcher():

	def __init__(self,ap):

		self.ap = ap
 
	def _dispatch(self, method, args):

		print "Dispatching " + method

		splitted = method.split(".")

		if splitted[0] == "mprisap":
			func = getattr(self.ap, splitted[1])
			return func(*args)	
		elif splitted[0] == "server":
			print "server"
			for server in self.ap.getMonitors():
				print "server: '"+server+"' : '"+splitted[1]+"'"
				if splitted[1] == server:
					for monitor in self.ap.getMonitors()[server]:
						print "Monitor to: " + monitor.getOrigin()
						if splitted[2] == "mpris":
							obj = MprisMprisProxy("org.mpris.MediaPlayer2."+server, self.ap.getBus())
							func = getattr(obj, splitted[3])
							return func(*args)
						elif splitted[2] == "player":
							obj = MprisPlayerProxy("org.mpris.MediaPlayer2."+server, self.ap.getBus())
							func = getattr(obj, splitted[3])
							return func(*args)
						elif splitted[2] == "tracklist":
							obj = MprisTrackListProxy("org.mpris.MediaPlayer2."+server, self.ap.getBus())
							func = getattr(obj, splitted[3])
							return func(*args)


class MprisServerMonitor:

    def __init__(self,server,origin,ap):

		print "MprisServerMonitor of " + server + " for " + origin

		props = DBUSHelper(dbus.SystemBus()).get_properties('org.freedesktop.hostname1', '/org/freedesktop/hostname1')
		hostname = str(props.Get('org.freedesktop.hostname1','Hostname'))

		self.ap = ap
		self.bus = ap.getBus() 
		self.server = str(server)
		self.origin = origin

		mpris_proxy = MprisMprisProxy(server, self.bus)
		player_proxy = MprisPlayerProxy(server, self.bus) 
		tracklist_proxy = MprisTrackListProxy(server, self.bus)

		identity_with_hostname = mpris_proxy.identity + " (on " + hostname + ")"

		jsonServer = jsonrpclib.Server(self.origin)
		jsonServer.mpris.onIdentity(self.server, identity_with_hostname, mpris_proxy.icon)

		playbackstatus = player_proxy.playbackstatus
		if playbackstatus is not None:
			self.__handle_playbackstatus(playbackstatus)	

		metadata = player_proxy.metadata
		if metadata is not None:
			self.__handle_metadata(metadata)
	
		tracklist = tracklist_proxy.tracklist
		if tracklist is not None:
			self.__handle_tracklist(tracklist)

		dbus_object = self.bus.get_object(server, '/org/mpris/MediaPlayer2')
		properties_manager = dbus.Interface(dbus_object, 'org.freedesktop.DBus.Properties')   
		self.prop_match = properties_manager.connect_to_signal("PropertiesChanged", self.__handle_properties_changed)

		jsonServer.mpris.onBackchannelInitialized(self.server)

    def shutdown(self):
		self.prop_match.remove()
		self.origin = None

    def getOrigin(self):
		return self.origin

    def getServer(self):
		return self.server

    def __handle_properties_changed(self, interface, changed, invalid):

		if self.origin is None:
			return

		if interface == "org.mpris.MediaPlayer2.Player":
			if "Metadata" in changed:
				metadata = changed.get("Metadata", None)
				if metadata is not None:
					self.__handle_metadata(metadata)
			
			if "PlaybackStatus" in changed:
				playbackstatus = changed.get("PlaybackStatus", None)
				if playbackstatus is not None:
					self.__handle_playbackstatus(playbackstatus)

		elif interface == "org.mpris.MediaPlayer2.TrackList":
			if "Tracks" in changed:
				tracklist_proxy = MprisTrackListProxy(self.server, self.bus)
				tracks = tracklist_proxy.tracklist
				if tracks is not None:
					self.__handle_tracklist(tracks)

    def __handle_tracklist(self, tracks):

		try:
			jsonServer = jsonrpclib.Server(self.origin)
			jsonServer.mpris.onTracklist(self.server, tracks) 
		except:
			pass #self.ap.removeMonitor(self.server,self.origin)	

    def __handle_playbackstatus(self, playbackstatus):

		try:
			jsonServer = jsonrpclib.Server(self.origin)
			jsonServer.mpris.onPlaybackStatus(self.server, str(playbackstatus))
		except:
			pass #self.ap.removeMonitor(self.server,self.origin)	
	
    def __handle_metadata(self, metadata):

		print "Sending got metadata to " + self.origin

		title = metadata.get("xesam:title")
		album = metadata.get("xesam:album")
		artist = metadata.get("xesam:artist")
		artUrl = metadata.get("mpris:artUrl")

		jsonServer = jsonrpclib.Server(self.origin)
		img = None 
	
		if artUrl is not None:

			try:
				if artUrl[:4]=="http":
					img = urllib2.urlopen(artUrl).read()

				elif artUrl[:4]=="file":

					with open(urllib2.unquote(artUrl[7:]), mode='rb') as file:
						img = file.read()

			except Exception, e:
				print e	

		if title is not None and artist is not None:

			if img is not None:
				try:
					jsonServer.mpris.onMetadata(self.server, str(title),str(artist[0]), base64.b64encode(img))
				except:
					pass #self.ap.removeMonitor(self.server,self.origin)	
			else:
				try:
					jsonServer.mpris.onMetadata(self.server, str(title),str(artist[0]), None)
				except:
					pass #self.ap.removeMonitor(self.server,self.origin)	

		print "Done sending metadata to " + self.origin

class MprisPlaybackStatusMonitor:
	
	def __init__(self, bus, server_to_watch, ap):
		self.bus = bus
		self.server = server_to_watch
		self.ap = ap
		self.playing = (MprisPlayerProxy(server_to_watch, bus).playbackstatus=="Playing")
		self.frozen = False

		print "Added PlaybackStatus monitor for " + server_to_watch

		dbus_object = self.bus.get_object(self.server, '/org/mpris/MediaPlayer2')
		properties_manager = dbus.Interface(dbus_object, 'org.freedesktop.DBus.Properties')   
		self.prop_match = properties_manager.connect_to_signal("PropertiesChanged", self.__handle_properties_changed)

	def freeze(self):
		self.frozen = True

	def thaw(self):
		self.frozen = False

	@property
	def is_playing(self):
		return self.playing

	def shutdown(self):
		self.prop_match.remove()

	def __handle_properties_changed(self, interface, changed, invalid):

		if self.frozen == True:
			return

		if interface == "org.mpris.MediaPlayer2.Player":

			if "PlaybackStatus" in changed:

				playbackstatus = changed.get("PlaybackStatus", None)

				if playbackstatus == "Playing": 

				    self.playing = True

				    for afPeer in self.ap.getAfPeers(): 

					try:
						print "Requesting Android AF from "+afPeer
						jsonServer = jsonrpclib.Server(afPeer)
						jsonServer.audiofocus.onServerPlaying(self.server)
						jsonServer.audiofocus.onRequestAudiofocus(self.server)
					except:
					 	self.ap.clearAfPeer(afPeer)	


				    real = self.server[23:]
				    except_list = [real]

				    self.ap.setLastPlaying(real)

				    self.ap.freeze()
				    self.ap.pauseLocal(except_list)
				    self.ap.thaw()

				else:
					
				    if self.server[23:] == self.ap.getLastPlaying():
					    for afPeer in self.ap.getAfPeers(): 

						try:
							jsonServer = jsonrpclib.Server(afPeer)
							jsonServer.audiofocus.onAbandonAudiofocus(self.server)
						except:
							self.ap.clearAfPeer(afPeer)	

class MprisAccessPoint:

	def __init__(self, bus):

	    self.monitors = {}
	    self.playback_status_monitors = {}

	    self.origin = []
	    self.afPeers = [] 
	    self.lastPlaying = None

	    self.bus = bus 

	    self.pactrl = None

	    try:
	    	self.pactrl = PaCtrl(self.bus, self)
	    except:
		pass

	    self.mpris_rpc_dispatcher = MprisRpcDispatcher(self)
	    self.__init_rpc()

	    self.b = self.bus.get_object('org.freedesktop.DBus','/org/freedesktop/DBus')
	    self.i = dbus.Interface(self.b, 'org.freedesktop.DBus')
	    self.i.connect_to_signal( "NameOwnerChanged", self.__bus_name_owner_changed )

	    self.ah = MprisAccessPointAvahiHelper(dbus.SystemBus(), self.port)

	    self.init_playbackstatus_monitors()

	def getAfPeers(self):
	    return self.afPeers

	def getMonitors(self):
	    return self.monitors

	def getBus(self):
	    return self.bus

	def getRpc(self):
            return self.rpc

	def getAvahi(self):
	    return self.ah

	def shutdown(self):
	    self.ah.shutdown()

	def __bus_name_owner_changed(self,name,old_owner,new_owner):

	    if len(self.origin) > 0 and name[:22]=="org.mpris.MediaPlayer2":

		    server = str(name)
		    real = server[23:]

		    if new_owner == '':

			    #TODO: Signal this to the origin; which also should be fault-tolerant regarding this and similar events

			    for monitor in self.monitors[real]:
				if monitor.getOrigin() in self.origin:
					monitor.shutdown()
					self.monitors[real].remove(monitor)				

			    self.playback_status_monitors[real].shutdown()
			    del self.playback_status_monitors[real]

			    for origin in self.origin:
				try:
					jsonServer = jsonrpclib.Server(origin)
					jsonServer.mprisap.onServerRemoved(server)
				except:
					pass

		    elif old_owner == '':
			    self.monitors[real] = []
			    self.playback_status_monitors[real]= MprisPlaybackStatusMonitor(self.bus, server, self)
			    mpris_proxy = MprisMprisProxy(server, self.bus)

			    print "Server " + mpris_proxy.identity + " added"

			    for origin in self.origin: 
				try:
					jsonServer = jsonrpclib.Server(origin)
					jsonServer.mprisap.onServerAdded(server, mpris_proxy.identity, mpris_proxy.icon)
				except:
					pass

	def discover(self,origin):

	    services = self.i.ListNames()
 	    jsonServer = jsonrpclib.Server(origin)

	    for s in services:
		if str(s).startswith('org.mpris.MediaPlayer2'):
			try:
				server = str(s)
				real = server[23:]

				if "." not in real:

					if real not in self.monitors:
						self.monitors[real] = []

					mpris_proxy = MprisMprisProxy(server, self.bus)

					try:
						jsonServer.mprisap.onServerAdded(server, mpris_proxy.identity, mpris_proxy.icon)
					except:
						pass

			except Exception, e:
				print e

	def init_playbackstatus_monitors(self):

	    services = self.i.ListNames()

	    playing_list = []

	    for s in services:

		server = str(s)

		if server.startswith('org.mpris.MediaPlayer2'):

			real = server[23:]

			if "." not in real:

				try:
					self.playback_status_monitors[real] = MprisPlaybackStatusMonitor(self.bus, server, self)
					player_proxy = MprisPlayerProxy(server, self.bus)
					if player_proxy.playbackstatus == "Playing":
						playing_list.append(real)

				except:
					pass

	    if len(playing_list) == 1:
		self.setLastPlaying(playing_list[0])

	def setAfPeer(self,afPeer):
		
            self.afPeers.append(afPeer)

	    print "Audiofocus peer added: " + afPeer

	    if self.lastPlaying is not None: 

		try:
	    		jsonServer = jsonrpclib.Server(afPeer)
			jsonServer.audiofocus.onServerPlaying("org.mpris.MediaPlayer2."+self.lastPlaying)

			player_proxy = MprisPlayerProxy("org.mpris.MediaPlayer2."+self.lastPlaying, self.bus)

			if player_proxy.playbackstatus == "Playing":
				jsonServer.audiofocus.onRequestAudiofocus("org.mpris.MediaPlayer2."+self.lastPlaying)
		except:
			self.clearAfPeer(afPeer)

        def clearAfPeer(self,afPeer):
	    self.afPeers.remove(afPeer)

	def getLastPlaying(self):
	    return self.lastPlaying

	def setLastPlaying(self,real):
	    self.lastPlaying = real

        def resumeLastPlaying(self):
	
	    if self.lastPlaying is not None:
		player_proxy = MprisPlayerProxy("org.mpris.MediaPlayer2."+self.lastPlaying, self.bus)
		print "RLP Status: " + player_proxy.playbackstatus
		if player_proxy.playbackstatus == "Paused":
			print "Resuming " + self.lastPlaying
			player_proxy.play_pause()

	def onPlaybackStatus(self,origin,status):

	    if status == "Playing": 

		self.freeze()
		self.pauseLocal()
		self.thaw()

	    elif status == "Paused":

		self.freeze()
		self.resumeLastPlaying()
		self.thaw()

	def freeze(self):
	    for real in self.playback_status_monitors:
		self.playback_status_monitors[real].freeze();

	def thaw(self):
	    for real in self.playback_status_monitors:
		self.playback_status_monitors[real].thaw();

	def pauseLocal(self,except_list=[]):

	    for real in self.playback_status_monitors:

		if real not in except_list: 

		    try:
			    print "Pausing " + real

			    player_proxy = MprisPlayerProxy("org.mpris.MediaPlayer2."+real, self.bus)
			    player_proxy.pause_always()

		    except:
			    pass

	def removeMonitor(self,server,origin):

	    real = str(server[23:]) 
	    print "Removing monitors for "+real+" with origin "+origin
	
            for monitor in self.monitors[real]:
		if monitor.getOrigin() == origin:
			monitor.shutdown()
			self.monitors[real].remove(monitor)

	def addMonitor(self,server,origin):

	    real = str(server[23:]) 
	    print "Adding monitor for "+real+" with origin "+origin

	    try:
		    monitor = MprisServerMonitor(server,origin,self)

		    if real not in self.monitors:
			self.monitors[real] = []

		    self.monitors[real].append(monitor)

		    print "...monitor added" 
	    except TypeError, e:
		    traceback.print_exc()

	def watch(self, origin):

	    self.origin.append(origin)
	    #self.discover(origin)

	def ignore(self, origin):

	    self.origin.remove(origin)

	def __init_rpc(self):

	    self.rpc = GlibJSONRPCServer(('', 0))
	    self.port = self.rpc.getPort()
	    self.rpc.add_allowed_ip('255.255.255.255')
	    self.rpc.register_instance(self.mpris_rpc_dispatcher)

