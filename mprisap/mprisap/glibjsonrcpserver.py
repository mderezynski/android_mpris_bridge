# Based on network.py from Sugar, Copyright (C) 2006-2007 Red Hat, Inc.
# Extended by Reinier Heeres for the QT Lab environment, (C)2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

import gobject
import SocketServer
import socket
import time
import re
import logging
import jsonrpclib
from jsonrpclib import Fault
from jsonrpclib.jsonrpc import USE_UNIX_SOCKETS
import SimpleXMLRPCServer
import SocketServer
import os
import types
import traceback
import sys

class GlibTCPServer():
    """GlibTCPServer

    Integrate socket accept into glib mainloop.
    """

    allow_reuse_address = True
    request_queue_size = 64

    def __init__(self, server_address, handler_class, allowed_ip=None):
        self._handler_class = handler_class
        self._allowed_ips = []
        if allowed_ip:
            self.add_allowed_ip(allowed_ip)

        self.socket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(server_address)
	self.port = self.socket.getsockname()[1]
        self.socket.setblocking(1)  # Set nonblocking

        # Watch the listener socket for data
        gobject.io_add_watch(self.socket, gobject.IO_IN, self._handle_accept)
        self.socket.listen(1)

    def getPort(self):
	return self.port

    def close(self):
        self.socket.close()

    def _handle_accept(self, source, condition):
        """Process incoming data on the server's socket by doing an accept()
        via handle_request()."""

        if not (condition & gobject.IO_IN):
            return True

        sock, addr = self.socket.accept()
        if not self.allow_client(addr[0]):
            logging.warning('Not allowing connection from %s', addr)
            sock.close()
            return
        logging.info('Allowing connection from %s', addr)

        handler = self._handler_class(sock, addr, self)
        return True

    def close_request(self, request):
        """Called to clean up an individual request."""
        pass

    def add_allowed_ip(self, ip_regexp):
        print 'Adding %s' % ip_regexp
        self._allowed_ips.append(re.compile(ip_regexp))

    def allow_client(self, client):
        for regexp in self._allowed_ips:
            m = regexp.match(client)
            return True
        return False

class GlibTCPHandler():
    '''
    Class to do asynchronous request handling integrated with GTK mainloop.
    '''

    BUFSIZE = 16384

    def __init__(self, sock, client_address, server, packet_len=False):
        '''
        If packet_len is True, the first 2 bytes received are expected to
        contain the packet length. The receive function will block until the
        whole packet is ready.
        '''

        if not isinstance(sock, socket.socket):
            raise ValueError('Only stream sockets supported')

        self._packet_len = packet_len
        self.socket = sock
        self.client_address = client_address
        self.server = server

        self.socket.setblocking(0)
        self._in_hid = None
        self._hup_hid = None
        # There seems to be an issue on Windows with the following...
        # self._hup_hid = gobject.io_add_watch(self.socket, \
        #    gobject.IO_ERR | gobject.IO_HUP, self._handle_hup)

        self.enable_callbacks()

    def enable_callbacks(self):
        if self._in_hid is not None:
            return
        self._in_hid = gobject.io_add_watch(self.socket, \
            gobject.IO_IN, self._handle_recv)

    def disable_callbacks(self):
        if self._in_hid is None:
            return
        gobject.source_remove(self._in_hid)
        self._in_hid = None

    def _handle_recv(self, socket, number):
        try:
            data = socket.recv(self.BUFSIZE)
        except Exception, e:
            # No data anyway...
            return True

        if len(data) == 0:
            self._handle_hup()
            return False
        else:
            self.handle(data)
            return True

        return True

    def _handle_hup(self, *args):
        if self._in_hid is not None:
            gobject.source_remove(self._in_hid)
            self._in_hid = None
        if self._hup_hid is not None:
            gobject.source_remove(self._hup_hid)
            self._hup_hid = None
        return False

    def handle(self, data):
        '''Override this function to handle actual data.'''
        print 'Data: %s, self: %r' % (data, repr(self))
        time.sleep(0.1)

    def finish(self):
        return


try:
    import fcntl
except ImportError:
    # For Windows
    fcntl = None

def get_version(request):
    # must be a dict
    if 'jsonrpc' in request.keys():
        return 2.0
    if 'id' in request.keys():
        return 1.0
    return None
    
def validate_request(request):
    if type(request) is not types.DictType:
        fault = Fault(
            -32600, 'Request must be {}, not %s.' % type(request)
        )
        return fault
    rpcid = request.get('id', None)
    version = get_version(request)
    if not version:
        fault = Fault(-32600, 'Request %s invalid.' % request, rpcid=rpcid)
        return fault        
    request.setdefault('params', [])
    method = request.get('method', None)
    params = request.get('params')
    param_types = (types.ListType, types.DictType, types.TupleType)
    if not method or type(method) not in types.StringTypes or \
        type(params) not in param_types:
        fault = Fault(
            -32600, 'Invalid request parameters or method.', rpcid=rpcid
        )
        return fault
    return True

class SimpleJSONRPCDispatcher(SimpleXMLRPCServer.SimpleXMLRPCDispatcher):

    def __init__(self, encoding=None):
        SimpleXMLRPCServer.SimpleXMLRPCDispatcher.__init__(self,
                                        allow_none=True,
                                        encoding=encoding)

    def _marshaled_dispatch(self, data, dispatch_method = None):
        response = None
        try:
            request = jsonrpclib.loads(data)
        except Exception, e:
            fault = Fault(-32700, 'Request %s invalid. (%s)' % (data, e))
            response = fault.response()
            return response
        if not request:
            fault = Fault(-32600, 'Request invalid -- no request data.')
            return fault.response()
        if type(request) is types.ListType:
            # This SHOULD be a batch, by spec
            responses = []
            for req_entry in request:
                result = validate_request(req_entry)
                if type(result) is Fault:
                    responses.append(result.response())
                    continue
                resp_entry = self._marshaled_single_dispatch(req_entry)
                if resp_entry is not None:
                    responses.append(resp_entry)
            if len(responses) > 0:
                response = '[%s]' % ','.join(responses)
            else:
                response = ''
        else:    
            result = validate_request(request)
            if type(result) is Fault:
                return result.response()
            response = self._marshaled_single_dispatch(request)
        return response

    def _marshaled_single_dispatch(self, request):
        # TODO - Use the multiprocessing and skip the response if
        # it is a notification
        # Put in support for custom dispatcher here
        # (See SimpleXMLRPCServer._marshaled_dispatch)
        method = request.get('method')
        params = request.get('params')
        try:
            response = self._dispatch(method, params)
        except:
            exc_type, exc_value, exc_tb = sys.exc_info()
            fault = Fault(-32603, '%s:%s' % (exc_type, exc_value))
            return fault.response()
        if 'id' not in request.keys() or request['id'] == None:
            # It's a notification
            return None
        try:
            response = jsonrpclib.dumps(response,
                                        methodresponse=True,
                                        rpcid=request['id']
                                        )
            return response
        except:
            exc_type, exc_value, exc_tb = sys.exc_info()
            fault = Fault(-32603, '%s:%s' % (exc_type, exc_value))
            return fault.response()

    def _dispatch(self, method, params):
        func = None
        try:
            func = self.funcs[method]
        except KeyError:
            if self.instance is not None:
                if hasattr(self.instance, '_dispatch'):
                    return self.instance._dispatch(method, params)
                else:
                    try:
                        func = SimpleXMLRPCServer.resolve_dotted_attribute(
                            self.instance,
                            method,
                            True
                            )
                    except AttributeError:
                        pass
        if func is not None:
            try:
                if type(params) is types.ListType:
                    response = func(*params)
                else:
                    response = func(**params)
                return response
            except TypeError:
                return Fault(-32602, 'Invalid parameters.')
            except:
                err_lines = traceback.format_exc().splitlines()
                trace_string = '%s | %s' % (err_lines[-3], err_lines[-1])
                fault = jsonrpclib.Fault(-32603, 'Server error: %s' % 
                                         trace_string)
                return fault
        else:
            return Fault(-32601, 'Method %s not supported.' % method)

class SimpleJSONRPCRequestHandler(
        SimpleXMLRPCServer.SimpleXMLRPCRequestHandler):
    
    def do_POST(self):
        if not self.is_rpc_path_valid():
            self.report_404()
            return
        try:
            max_chunk_size = 10*1024*1024
            size_remaining = int(self.headers["content-length"])
            L = []
            while size_remaining:
                chunk_size = min(size_remaining, max_chunk_size)
                L.append(self.rfile.read(chunk_size))
                size_remaining -= len(L[-1])
            data = ''.join(L)
            response = self.server._marshaled_dispatch(data)
            self.send_response(200)
        except Exception, e:
            self.send_response(500)
            err_lines = traceback.format_exc().splitlines()
            trace_string = '%s | %s' % (err_lines[-3], err_lines[-1])
            fault = jsonrpclib.Fault(-32603, 'Server error: %s' % trace_string)
            response = fault.response()
        if response == None:
            response = ''
        self.send_header("Content-type", "application/json-rpc")
        self.send_header("Content-length", str(len(response)))
        self.end_headers()
        self.wfile.write(response)
        self.wfile.flush()
        self.connection.shutdown(1)


class GlibJSONRPCServer(GlibTCPServer, SimpleJSONRPCDispatcher):

    allow_reuse_address = True

    def __init__(self, addr, requestHandler=SimpleJSONRPCRequestHandler,
                 logRequests=True, encoding=None, bind_and_activate=True,
                 address_family=socket.AF_INET6):
        self.logRequests = logRequests
        SimpleJSONRPCDispatcher.__init__(self, encoding)
        # TCPServer.__init__ has an extra parameter on 2.6+, so
        # check Python version and decide on how to call it
        vi = sys.version_info
        self.address_family = address_family
        if USE_UNIX_SOCKETS and address_family == socket.AF_UNIX:
            # Unix sockets can't be bound if they already exist in the
            # filesystem. The convention of e.g. X11 is to unlink
            # before binding again.
            if os.path.exists(addr): 
                try:
                    os.unlink(addr)
                except OSError:
                    logging.warning("Could not unlink socket %s", addr)
        # if python 2.5 and lower
        GlibTCPServer.__init__(self, addr, requestHandler)
