package net.mderezynski.amb.ui;

import android.content.Context;
import android.content.Intent;

import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;

import net.mderezynski.amb.R;

import org.json.rpc.commons.JsonErrorListener;
import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.discovery.MprisApHostInfo;
import org.mpris.bridge.android.discovery.MprisDiscoveryManager;
import org.mpris.bridge.android.app.MprisBaseDiscoveryActivity;
import org.mpris.bridge.android.impl.MprisAudioFocusService;
import org.mpris.bridge.android.impl.MprisBonjourService;
import org.mpris.bridge.android.impl.MprisNotificationService;
import org.mpris.bridge.android.impl.MprisServiceWatchdogService;
import org.mpris.bridge.android.interfaces.MprisAccessPointBackchannelListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A login screen that offers login via email/password.

 */
public class MprisDiscoveryActivity extends MprisBaseDiscoveryActivity {

    private ServerDiscoveryListener serverDiscoveryListener;
    private AccessPointAdapter accessPointDiscoveryListener;
    private MprisDiscoveryManager discoveryManager;
    private Spinner accessPointSpinner;
    private GridView serverList;

    private class El
            implements JsonErrorListener {

        @Override
        public void onTransportError(String method, Object[] params) {

            /*
            final Uri errorOrigin = Uri.parse(apUrl);

            // we simply remove the AP from the list, for the moment -- md

            for(int n = 0 ; n < accessPointDiscoveryListener.getCount(); n++) {
                MprisApHostInfo item = accessPointDiscoveryListener.getItem(n);
                if(item.uri.equals(errorOrigin)) {
                    accessPointDiscoveryListener.removeApAt(n);
                    break;
                }
            } */
                   }
    }

    @Override
    public JsonErrorListener getErrorListener() {
        return new El();
    }

    private class AccessPointAdapter extends BaseAdapter implements MprisDiscoveryManager.DiscoveryListener {

        private final List<MprisApHostInfo> accessPoints = new ArrayList<MprisApHostInfo>();
        private final Spinner accessPointSpinner;

        public AccessPointAdapter(final Spinner spinner) {
            this.accessPointSpinner = spinner;
        }

        @Override
        public void onDiscoveryStarted() {
        }

        @Override
        public void onDiscoveryStopped() {
        }

        @Override
        public void onAccessPointDiscovered(MprisApHostInfo info) {

            delMprisAp(info);
            addMprisAp(info);

        }

        @Override
        public void onAccessPointDisappeared(MprisApHostInfo info) {
            Log.d("MprisBonjour", "MPRIS AP Disappeared: " + info.uri);

            if(accessPointSpinner.getSelectedItemPosition()>Spinner.INVALID_POSITION&&mprisApExists(info)) {
                MprisApHostInfo currentInfo = getItem(accessPointSpinner.getSelectedItemPosition());
                if(currentInfo!=null&&currentInfo.hostName.equals(info.hostName)) {
                    stopProxy();
                }
            }

            delMprisAp(info);
        }

        public void replaceMprisAp(final MprisApHostInfo info) {
            int index = 0;
            for(MprisApHostInfo ap:accessPoints) {
                if(info.hostName.equals(ap.hostName)) {
                    accessPoints.set(index, info);
                    break;
                }
                index++;
            }
            notifyDataSetChanged();
        }

        public void addMprisAp(final MprisApHostInfo info) {
            accessPoints.add(info);
            notifyDataSetChanged();
        }

        public void delMprisAp(final MprisApHostInfo info) {
            int index = 0;

            // workaround loop because MprisApHostInfo.equals() is not fully correct

            for(MprisApHostInfo ap:accessPoints) {
                if(info.hostName.equals(ap.hostName)) {
                    accessPoints.remove(index);
                    break;
                }
                index++;
            }

            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return accessPoints.size();
        }

        @Override
        public MprisApHostInfo getItem(int i) {
            return accessPoints.get(i);
        }

        @Override
        public long getItemId(int i) {

            if(i<0 || i >= accessPoints.size())
                return -1;

            return accessPoints.indexOf(accessPoints.get(i));
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            if(view==null) {
                view = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(android.R.layout.simple_list_item_1, viewGroup, false);
            }

            ((TextView)view.findViewById(android.R.id.text1)).setText(((MprisApHostInfo)getItem(i)).hostName);

            return view;
        }

        public boolean mprisApExists(final MprisApHostInfo info) {
            return accessPoints.contains(info);
        }

        public void removeApAt(int n) {
            accessPoints.remove(n);
            notifyDataSetChanged();
        }

        public void clear() {
            accessPoints.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discovery);

        MprisServiceWatchdogService.start(getApplicationContext());

        serverList = (GridView) findViewById(R.id.services);
        accessPointSpinner = (Spinner) findViewById(R.id.spinner2);

        serverList.setOnItemClickListener(new AbsListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                ServerDiscoveryListener.HostInfo serverListAdapterHostInfo = serverDiscoveryListener.getItem(i);
                MprisApHostInfo info = accessPointDiscoveryListener.getItem(accessPointSpinner.getSelectedItemPosition());

                Intent intent = new Intent(getApplicationContext(), MprisControlActivity.class);
                intent.putExtra(Constants.MPRIS_DATA_SERVICE, serverListAdapterHostInfo.server);
                intent.putExtra(Constants.MPRIS_DATA_MPRISAP_URL, info.uri.toString());
                startActivity(intent);

            }
        });

        accessPointSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                serverDiscoveryListener.setCurrentAp(i);
                serverDiscoveryListener.clear();

                MprisApHostInfo info = accessPointDiscoveryListener.getItem(i);

                if(switchProxy(info.uri.toString())) {
                    Intent intent = new Intent(getApplicationContext(), MprisAudioFocusService.class);
                    intent.setAction(Constants.MPRIS_AF_SERVICE_CONNECT);
                    intent.putExtra(Constants.MPRIS_DATA_AP_HOST_INFO, info);
                    startService(intent);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                serverDiscoveryListener.clear();
            }
        });

        accessPointDiscoveryListener = new AccessPointAdapter(accessPointSpinner);
        serverDiscoveryListener = new ServerDiscoveryListener((LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE));

        com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter siaa = new ScaleInAnimationAdapter(serverDiscoveryListener);
        siaa.setAbsListView(serverList);

        serverList.setAdapter(siaa);
        accessPointSpinner.setAdapter(accessPointDiscoveryListener);

        discoveryManager = new MprisDiscoveryManager(getApplicationContext(), accessPointDiscoveryListener);
    }


    @Override
    protected MprisAccessPointBackchannelListener getAccessPointListener() {
        return serverDiscoveryListener;
    }

    @Override
    protected void onResume() {
        super.onResume();
        discoveryManager.start(getApplicationContext());
        MprisBonjourService.startDiscovery(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopProxy();
        discoveryManager.stop(getApplicationContext());
        accessPointDiscoveryListener.clear();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        discoveryManager.shutdown(getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.shutdown) {
            MprisBonjourService.stop(getApplicationContext());
            Intent stopIntentNS = new Intent(getApplicationContext(), MprisNotificationService
                    .class);
            stopService(stopIntentNS);
            Intent stopIntentAF = new Intent(getApplicationContext(), MprisAudioFocusService
                    .class);
            stopService(stopIntentAF);
            finish();
            return true;
        }

        return false;
    }
}




