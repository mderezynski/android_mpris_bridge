package net.mderezynski.amb.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Base64;
import android.util.Base64InputStream;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.mderezynski.amb.R;

import org.mpris.bridge.android.impl.MprisServerRpcService;
import org.mpris.bridge.android.interfaces.MprisAccessPointBackchannelListener;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TrackListAdapter extends BaseListAdapter {

    private final int LAYOUT_RESOURCE_ID = android.R.layout.simple_list_item_1;
    private List<String> data = new ArrayList<String>();


    public TrackListAdapter(final LayoutInflater inflater) {
        super(inflater);
    }

    @Override
    public final View newView(ViewGroup viewGroup) {
        View view = mInflater.inflate(LAYOUT_RESOURCE_ID, viewGroup, false);
        ((TextView)view.findViewById(android.R.id.text1)).setTextColor(Color.BLACK);
        return view;
    }

    @Override
    public final void bindView(int position, View view) {
        ((TextView) view.findViewById(android.R.id.text1)).setText(data.get(position));
    }

    @Override
    public int getCount() {
        return data.size();
    }

    public void setData(ArrayList<String> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public String getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void clear() {
        notifyDataSetChanged();
    }
}