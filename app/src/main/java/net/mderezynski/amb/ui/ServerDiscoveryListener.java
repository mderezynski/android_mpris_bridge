package net.mderezynski.amb.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Base64InputStream;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.mderezynski.amb.R;

import org.mpris.bridge.android.interfaces.MprisAccessPointBackchannelListener;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ServerDiscoveryListener extends BaseListAdapter implements MprisAccessPointBackchannelListener {

    private final int LAYOUT_RESOURCE_ID = R.layout.activity_discovery_server_list_item;
    private List<HostInfo> data = new ArrayList<HostInfo>();


    public ServerDiscoveryListener(final LayoutInflater inflater) {
        super(inflater);
    }

    public static class HostInfo implements Comparable<HostInfo> {

        private static int idOrigin = 0;

        Bitmap      icon;
        String      identity;
        String      server;
        int         id;

        public HostInfo() {
            this.id = ++idOrigin;
        }

        @Override
        public boolean equals(final Object other) {

            if(!(other instanceof HostInfo))
                return false;

            final HostInfo otherHostInfo = (HostInfo) other;

            return otherHostInfo.server.equals(server);
        }

        @Override
        public int compareTo(HostInfo hostInfo) {
            return identity.compareTo(hostInfo.identity);
        }
    }

    @Override
    public void onServerRemoved(final String server) {

        Log.d("MprisBonjour", "Removed server " + server);
        final HostInfo info = new HostInfo();
        info.server = server;
        data.remove(info);
        notifyDataSetChanged();

    }

    @Override
    public void onServerAdded(final String server, final String identity, final String iconBase64) {

        final ServerDiscoveryListener.HostInfo info = new ServerDiscoveryListener.HostInfo();
        info.server = server;
        info.identity = identity;

        if(data.contains(info))
            return;

        InputStream stream = null;

        try {
            stream = new ByteArrayInputStream(iconBase64.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Base64InputStream is = new Base64InputStream(stream, Base64.DEFAULT);

        final Bitmap icon = BitmapFactory.decodeStream(is);


        info.icon = icon;

        data.add(info);
        Log.d("MprisBonjour","Added server "+server);
        notifyDataSetChanged();

    }

    private Integer currentAp = null;

    public Integer getCurrentAp() {
        return currentAp;
    }

    public void setCurrentAp(Integer currentAp) {
        this.currentAp = currentAp;
    }

    @Override
    public final View newView(ViewGroup viewGroup) {

        return mInflater.inflate(LAYOUT_RESOURCE_ID, viewGroup, false);
    }

    @Override
    public final void bindView(int position, View view) {

        final HostInfo info = getItem(position);

        ((TextView) view.findViewById(R.id.identity)).setText(info.identity);
        ((ImageView) view.findViewById(R.id.appicon)).setImageBitmap(info.icon);

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public HostInfo getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return data.get(i).id;
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetChanged() {
        Collections.sort(data);
        super.notifyDataSetChanged();
    }
}