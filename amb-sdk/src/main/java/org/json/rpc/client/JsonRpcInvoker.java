 /*
 * Copyright (C) 2011 ritwik.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.json.rpc.client;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

import org.json.rpc.commons.JsonRpcClientException;
import org.json.rpc.commons.JsonRpcRemoteException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class JsonRpcInvoker {

    private static final Logger LOG = LoggerFactory.getLogger(JsonRpcInvoker.class);

    private final Random rand = new Random();

    public JsonRpcInvoker() {
    }

    public <T extends Object> T get(final JsonRpcClientTransport transport, final String handle,
                      final Class<?>... classes) {

        return (T) Proxy.newProxyInstance(JsonRpcInvoker.class.getClassLoader(), classes,
                new InvocationHandler() {
            public T invoke(Object proxy, Method method, Object[] args) throws Throwable {
                    return (T) JsonRpcInvoker.this.invoke(handle, transport, method, args);
            }
        });
    }

    public static class JsonRequest {

        public String jsonrpc;
        public String id;
        public String method;
        public Object[] params;

    }

    public static class JsonResult {

        public String jsonrpc;
        public String id;
        public String error;

    }


    public static class JsonResponse {

        public String jsonrpc;
        public Object result;
        public String id;

    }


    public static ObjectMapper getNewObjectMapper() {

        ObjectMapper mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(new PropertyNamingStrategy
                .LowerCaseWithUnderscoresStrategy());

        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return mapper;
    }

    private Object invoke(String handleName,
                          JsonRpcClientTransport transport, Method method,
                          Object[] args) throws JsonRpcClientException, IOException {

        int id = rand.nextInt(Integer.MAX_VALUE);
        String methodName = handleName + "." + method.getName();

        JsonRequest req = new JsonRequest();
        req.jsonrpc = "2.0";
        req.id = String.valueOf(id);
        req.method = methodName;

        if(args==null)
            args = new Object[0];

        req.params = args;

        ObjectMapper mapper = getNewObjectMapper();

        String requestData = mapper.writeValueAsString(req);

        LOG.debug("JSON-RPC >>  {}", requestData);
        String responseData = "";
        try {
            responseData = transport.call(requestData);
        } catch (Exception e) {
            try {
                if(method.getReturnType()==Void.class||method.getReturnType()==void.class) {
                    return null;
                }
                return method.getReturnType().newInstance();
            } catch (InstantiationException e1) {
                e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                e1.printStackTrace();
            }
        }
        LOG.debug("JSON-RPC <<  {}", responseData);

        JsonResult result = null;
        try {
            result = mapper.readValue(responseData, JsonResult.class);
        } catch( JsonMappingException e ) {

        }

        if( result == null ) {
            throw new JsonRpcRemoteException("could not parse response");
        }

        if (result.error != null) {
            throw new JsonRpcRemoteException("unknown error, data = " + result.error);
        }

        if (method.getReturnType() == void.class) {
            return null;
        }

        return mapper.readValue(result.toString(), method.getReturnType());
    }
}
