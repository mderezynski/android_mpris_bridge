/*
 * Copyright (C) 2011 ritwik.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.json.rpc.client;

import android.net.Uri;

import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.net.SocketFactory;

public class HttpJsonRpcClientTransport implements JsonRpcClientTransport {

    private Uri url;
    private final Map<String, String> headers;

    public HttpJsonRpcClientTransport(Uri url) {
        this.url = url;
        this.headers = new HashMap<String, String>();
    }

    public final void setHeader(String key, String value) {
        this.headers.put(key, value);
    }

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    public final String call(String requestData) throws Exception {
        String responseData = post(url.toString(), headers, requestData);
        return responseData;
    }

    private String post(String url, Map<String, String> headers, String data)
            throws IOException {

        OkHttpClient client = new OkHttpClient();

        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
        NetworkInterface wifiInterface = null;
        while (networkInterfaces.hasMoreElements()) {
            NetworkInterface networkInterface = networkInterfaces.nextElement();
            if (networkInterface.getDisplayName().equals("wlan0") || networkInterface.getDisplayName().equals("eth0")) {
                wifiInterface = networkInterface;
                break;
            }
        }

        final Enumeration<InetAddress> src = wifiInterface.getInetAddresses();

        client.setSocketFactory(new SocketFactory() {
            @Override
            public Socket createSocket(String s, int i) throws IOException, UnknownHostException {
                return new Socket(s, i);
            }

            @Override
            public Socket createSocket(String s, int i, InetAddress inetAddress, int i2) throws IOException, UnknownHostException {

                InetAddress ipv6 = null;
                InetAddress ipv4 = null;

                while(src.hasMoreElements()) {
                    InetAddress addr = src.nextElement();
                    if(addr instanceof Inet6Address) {
                        ipv6 = addr;
                    } else if(addr instanceof Inet4Address) {
                        ipv4 = addr;
                    }
                }

                return new Socket(s, i, (ipv6==null)?ipv4:ipv6, i2);
            }

            @Override
            public Socket createSocket(InetAddress inetAddress, int i) throws IOException {
                return new Socket(inetAddress, i);
            }

            @Override
            public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) throws IOException {

                InetAddress ipv6 = null;
                InetAddress ipv4 = null;

                while(src.hasMoreElements()) {
                    InetAddress addr = src.nextElement();
                    if(addr instanceof Inet6Address) {
                        ipv6 = addr;
                    } else if(addr instanceof Inet4Address) {
                        ipv4 = addr;
                    }
                }

                return new Socket(inetAddress, i, (ipv6==null)?ipv4:ipv6, i2);
            }
        });

        Headers.Builder builder = new Headers.Builder();

        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                builder.add(entry.getKey(), entry.getValue());
            }
        }

       RequestBody body = RequestBody.create(JSON, data);
       Request request = new Request.Builder()
                .url(url)
                .post(body)
                .headers(builder.build())
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
