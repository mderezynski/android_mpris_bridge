package org.json.rpc.commons;


import org.mpris.bridge.android.discovery.MprisApHostInfo;

public interface JsonErrorListener {

    public void onTransportError(final String method, final Object[] params);

}
