package org.mpris.bridge.android.interfaces;

import android.graphics.Bitmap;

/**
 * Created by milosz on 03.10.14.
 */
public interface MprisAccessPointBackchannelDispatcher {

    public abstract void gotServerRemoved(final String ap, final String server);
    public abstract void gotServerAdded(final String ap, final String server);

}
