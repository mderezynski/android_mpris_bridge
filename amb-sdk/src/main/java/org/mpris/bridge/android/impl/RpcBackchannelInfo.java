package org.mpris.bridge.android.impl;

/**
* Created by milosz on 04.10.14.
*/
public class RpcBackchannelInfo<T extends Object,I extends Class<?>> {

    final T       marshaller;
    final I       iface;
    final String  ifaceRemoteName;

    public RpcBackchannelInfo(final T m, final I i, final String name) {
        this.ifaceRemoteName = name;
        this.iface = i;
        this.marshaller = m;
    }
}
