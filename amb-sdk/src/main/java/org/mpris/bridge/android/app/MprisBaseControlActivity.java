package org.mpris.bridge.android.app;

import android.app.Activity;
import android.os.Bundle;

import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.impl.RpcBackchannelInfo;
import org.mpris.bridge.android.proxy.MprisServerProxy;
import org.mpris.bridge.android.interfaces.MprisServerBackchannelListener;
import org.mpris.bridge.android.interfaces.MprisServerBackchannelMarshaller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by milosz on 03.10.14.
 */
public abstract class MprisBaseControlActivity extends Activity implements JsonErrorHandlerImplementer {

    private MprisServerProxy proxy;
    private boolean resumed = false;

    protected abstract MprisServerBackchannelMarshaller getListener(final String server);

    public MprisBaseControlActivity() {
    }

    public final MprisServerProxy getProxy() {
        return proxy;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final String apUrl = getIntent().getStringExtra(Constants.MPRIS_DATA_MPRISAP_URL);
        final String server = getIntent().getStringExtra(Constants.MPRIS_DATA_SERVICE);

        final RpcBackchannelInfo info =
          new RpcBackchannelInfo(getListener(server),
                  MprisServerBackchannelListener.class,
                  Constants.MPRIS_RPC_SERVER_INTERFACE);

        final List<RpcBackchannelInfo> infos =
          new ArrayList<RpcBackchannelInfo>();

        infos.add(info);

        try {
            proxy = new MprisServerProxy(getApplicationContext(), infos, apUrl, server, getErrorListener());
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(resumed)
            proxy.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        resumed = true;
        proxy.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        resumed = false;
        proxy.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        proxy.shutdown();
    }
}
