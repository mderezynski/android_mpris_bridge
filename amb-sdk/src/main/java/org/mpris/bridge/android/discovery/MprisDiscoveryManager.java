package org.mpris.bridge.android.discovery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;

import org.mpris.bridge.android.Constants;

/**
 * Created by milosz on 01.10.14.
 */
public class MprisDiscoveryManager {

    private Handler handler;

    private BroadcastReceiver receiver2 = new BroadcastReceiver() {

        @Override
        public void onReceive(
                Context context, Intent intent) {

            final MprisApHostInfo info = intent.getParcelableExtra(Constants.MPRIS_DATA_AP_HOST_INFO);

            handler.post(new Runnable() {
                @Override
                public void run() {
                    discoveryListener.onAccessPointDiscovered(info);
                }
            });

        }
    };

    private BroadcastReceiver receiver3 = new BroadcastReceiver() {

        @Override
        public void onReceive(
                Context context, Intent intent) {

            final MprisApHostInfo info = intent.getParcelableExtra(Constants.MPRIS_DATA_AP_HOST_INFO);

            handler.post(new Runnable() {
                @Override
                public void run() {
                    discoveryListener.onAccessPointDisappeared(info);
                }
            });

        }
    };


    public static interface DiscoveryListener {

        public void onDiscoveryStarted();
        public void onDiscoveryStopped();

        public void onAccessPointDiscovered(final MprisApHostInfo info);
        public void onAccessPointDisappeared(final MprisApHostInfo info);
    }

    private DiscoveryListener discoveryListener;

    public MprisDiscoveryManager(final Context ctx, final DiscoveryListener listener) {
        discoveryListener = listener;
        handler = new Handler(Looper.getMainLooper());
    }

    public void shutdown(final Context ctx) {
    }

    public void start(final Context ctx) {

        discoveryListener.onDiscoveryStarted();

        LocalBroadcastManager.getInstance(ctx).registerReceiver(receiver2,
                new IntentFilter(Constants.MPRIS_SERVICE_RESULT_AP_DISCOVERED));

        LocalBroadcastManager.getInstance(ctx).registerReceiver(receiver3,
                new IntentFilter(Constants.MPRIS_SERVICE_RESULT_AP_DISAPPEARED));

    }

    public void stop(final Context ctx) {

        LocalBroadcastManager.getInstance(ctx).unregisterReceiver(receiver2);
        LocalBroadcastManager.getInstance(ctx).unregisterReceiver(receiver3);

        discoveryListener.onDiscoveryStopped();
    }

}
