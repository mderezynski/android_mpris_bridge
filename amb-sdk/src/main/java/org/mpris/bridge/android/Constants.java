package org.mpris.bridge.android;

/**
 * Created by milosz on 01.10.14.
 */
public class Constants {
    public final static String MPRIS_PKG_PREFIX = "org.mpris.bridge.android";

    public final static String MPRIS_DATA_MPRISAP_URL = MPRIS_PKG_PREFIX + ".data.MPRISAP_URL";
    public final static String MPRIS_DATA_SERVICE = MPRIS_PKG_PREFIX + ".data.SERVICE";
    public final static String MPRIS_DATA_ADDRESS = MPRIS_PKG_PREFIX + ".data.ADDRESS";
    public final static String MPRIS_DATA_PORT = MPRIS_PKG_PREFIX + ".data.PORT";
    public final static String MPRIS_DATA_AP_HOST_INFO = MPRIS_PKG_PREFIX + ".data.AP_HOST_INFO";

    public final static String MPRIS_SERVICE_ACTION_VOLUME = MPRIS_PKG_PREFIX + ".action.ACTION_VOLUME";
    public final static String MPRIS_SERVICE_ACTION_PREVIOUS = MPRIS_PKG_PREFIX + ".action.ACTION_PREVIOUS";
    public final static String MPRIS_SERVICE_ACTION_NEXT = MPRIS_PKG_PREFIX + ".action.ACTION_NEXT";
    public final static String MPRIS_SERVICE_ACTION_PLAYPAUSE = MPRIS_PKG_PREFIX + ".action.ACTION_PLAYPAUSE";
    public final static String MPRIS_SERVICE_ACTION_RAISE = MPRIS_PKG_PREFIX + ".action.ACTION_RAISE";
    public final static String MPRIS_SERVICE_ACTION_TRACKLIST = MPRIS_PKG_PREFIX + ".action.ACTION_TRACKLIST";
    public static final String MPRIS_SERVICE_ACTION_GO_TO = MPRIS_PKG_PREFIX + ".action.ACTION_GO_TO";

    public final static String MPRIS_BASE_CONTROL_SERVICE_ACTION_START = MPRIS_PKG_PREFIX + ".action.ACTION_START";
    public final static String MPRIS_ACTION_TERMINATE_NOTIFICATION_SERVICE = MPRIS_PKG_PREFIX + ".action.ACTION_TERMINATE_NOTIFICATION_SERVICE";

    public final static String MPRIS_AP_ACTION_MONITOR_SERVER = MPRIS_PKG_PREFIX + ".action.ACTION_MONITOR_SERVER";
    public final static String MPRIS_AP_ACTION_REMOVE_MONITOR = MPRIS_PKG_PREFIX + ".action.ACTION_REMOVE_MONITOR";
    public final static String MPRIS_AP_ACTION_DISCOVER = MPRIS_PKG_PREFIX + ".action.ACTION_DISCOVER";
    public final static String MPRIS_AP_ACTION_WATCH = MPRIS_PKG_PREFIX + ".action.ACTION_WATCH";
    public final static String MPRIS_AP_ACTION_IGNORE = MPRIS_PKG_PREFIX + ".action.ACTION_IGNORE";
    public final static String MPRIS_AP_ACTION_PBSTATUS_PLAYING = MPRIS_PKG_PREFIX + ".action.ACTION_PAUSE_ALL";
    public final static String MPRIS_AP_ACTION_SET_AF_PEER = MPRIS_PKG_PREFIX + ".action.ACTION_SET_AF_PEER";
    public final static String MPRIS_AP_ACTION_CLEAR_AF_PEER = MPRIS_PKG_PREFIX + ".action.ACTION_CLEAR_AF_PEER";
    public final static String MPRIS_AP_ACTION_PBSTATUS_PAUSED = MPRIS_PKG_PREFIX + ".action.ACTION_RESUME_LAST_PLAYING";

    public final static String MPRIS_DISCOVER_ACTION_START = MPRIS_PKG_PREFIX + ".action.ACTION_START_DISCOVERY";
    public final static String MPRIS_DISCOVER_ACTION_STOP = MPRIS_PKG_PREFIX + ".action.ACTION_STOP_DISCOVERY";
    public final static String MPRIS_DISCOVER_ACTION_CLEAR_CACHE = MPRIS_PKG_PREFIX + ".action.ACTION_CLEAR_CACHE";

    public final static String MPRIS_DISCOVER_ACTION_NOTIFY_STARTED = MPRIS_PKG_PREFIX + ".action.ACTION_DISCOVER_NOTIFY_STARTED";
    public static final String MPRIS_DISCOVER_ACTION_NOTIFY_STOPPED = MPRIS_PKG_PREFIX + ".action.ACTION_DISCOVER_NOTIFY_STOPPED";

    public final static String MPRIS_SERVICE_RESULT_AP_DISCOVERED = MPRIS_PKG_PREFIX + ".action.ACTION_AP_DISCOVERED";
    public final static String MPRIS_SERVICE_RESULT_AP_DISAPPEARED = MPRIS_PKG_PREFIX + ".action.ACTION_AP_DISAPPEARED";

    public final static String MPRIS_AF_SERVICE_CONNECT = MPRIS_PKG_PREFIX + ".action.ACTION_AF_CONNECT";
    public static final String MPRIS_AF_SERVICE_NOTIFY_CONNECTED = MPRIS_PKG_PREFIX + ".action.ACTION_AF_NOTIFY_CONNECTED";
    public static final String MPRIS_AF_SERVICE_NOTIFY_STOPPED = MPRIS_PKG_PREFIX + ".action.ACTION_AF_NOTIFY_STOPPED";

    public final static String MPRIS_RPC_AP_INTERFACE = "mprisap";
    public final static String MPRIS_RPC_SERVER_INTERFACE = "mpris";
    public final static String MPRIS_RPC_AF_INTERFACE = "audiofocus";
}
