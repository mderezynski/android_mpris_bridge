package org.mpris.bridge.android.impl;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.json.rpc.commons.JsonErrorListener;
import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.app.MprisBaseControlService;
import org.mpris.bridge.android.interfaces.MprisServerBackchannelMarshaller;

import java.util.ArrayList;

/**
 * Created by milosz on 06.10.14.
 */
public class MprisNotificationService extends MprisBaseControlService {

    private Notification n;
    private Bitmap curCoverArt;
    private Bitmap curIdentityIcon;
    private String curTitle;
    private String curArtist;

    private BroadcastReceiver receiver1 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            stopSelf();
        }
    };

    private class El
            implements JsonErrorListener {

        @Override
        public void onTransportError(String method, Object[] params) {
        }
    }

    @Override
    public JsonErrorListener getErrorListener() {
        return new El();
    }

    private class ServerListener extends MprisFilteringServerDispatcher {

        public ServerListener(String server) {
            super(server);
        }

        @Override
        public void gotBackchannelInitialized() {
        }

        @Override
        public void gotMetadata(final String title, final String artist, final Bitmap coverArt) {
            curTitle = title;
            curArtist = artist;
            curCoverArt = coverArt;
            notifyUpdateNotification();

        }

        @Override
        public void gotIdentity(final String identity, final Bitmap icon) {
            curIdentityIcon = icon;
            notifyUpdateNotification();
        }

        @Override
        public void gotPlaybackStatus(final String playbackStatus) {
            pbStatus = playbackStatus;
            notifyUpdateNotification();
        }

        @Override
        public void gotTracklist(ArrayList<String> tracks) {

        }
    }

    private String pbStatus;

    private void notifyUpdateNotification() {

        if(pbStatus==null)
            return;

        NotificationCompat.Builder b = new NotificationCompat.Builder(this);

        MediaPlayerNotificationStyle style = new MediaPlayerNotificationStyle(
                this,
                curArtist, curTitle, curCoverArt, curIdentityIcon,
                getResources().getDisplayMetrics().densityDpi,
                getProxy().getName(), getProxy().getApUrl(), pbStatus);

        n = style.build(b);
        startForeground(2, n);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver1, new IntentFilter(Constants.MPRIS_DISCOVER_ACTION_NOTIFY_STOPPED));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(receiver1);
        getProxy().shutdown();
        stopForeground(true);
    }

    @Override
    protected MprisServerBackchannelMarshaller getListener(String server) {
        return new ServerListener(server);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onProxyInitialized() {
        notifyUpdateNotification();
    }



}
