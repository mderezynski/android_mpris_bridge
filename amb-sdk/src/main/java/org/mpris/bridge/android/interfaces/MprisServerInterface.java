package org.mpris.bridge.android.interfaces;

import org.json.rpc.commons.JsonRpcClientException;

/**
 * Created by milosz on 29.09.14.
 */
public interface MprisServerInterface {

    public void playPause() throws JsonRpcClientException;
    public void next() throws JsonRpcClientException;
    public void previous() throws JsonRpcClientException;
    public void volume(double volume) throws JsonRpcClientException;

}
