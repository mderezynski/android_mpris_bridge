package org.mpris.bridge.android.exceptions;

import java.io.IOException;

/**
 * Created by milosz on 29.09.14.
 */
public class UnableToPerformDiscoveryException extends IOException {
    public UnableToPerformDiscoveryException() {
        super();
    }
}
