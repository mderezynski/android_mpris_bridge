package org.mpris.bridge.android.proxy;

import android.content.Context;

import org.json.rpc.commons.JsonErrorListener;
import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.impl.MprisAccessPointRpcService;
import org.mpris.bridge.android.impl.RpcBackchannelInfo;

import java.io.IOException;
import java.util.List;

/**
 * Created by milosz on 29.09.14.
 */
public class MprisAccessPointProxy extends MprisAbsProxy {

    public MprisAccessPointProxy(final Context ctx, final List<RpcBackchannelInfo> infos,
                                 final String apUrl, final JsonErrorListener l) throws IOException {

        super(ctx, infos, apUrl, l);
    }

    private void init() {
        MprisAccessPointRpcService.doAction(getContext(),
                Constants.MPRIS_AP_ACTION_DISCOVER, getApUrl(), getPort());
    }

    private void watch() {
        MprisAccessPointRpcService.doAction(getContext(),
                Constants.MPRIS_AP_ACTION_WATCH, getApUrl(), getPort());
    }

    private void ignore() {
        MprisAccessPointRpcService.doAction(getContext(),
                Constants.MPRIS_AP_ACTION_IGNORE, getApUrl(), getPort());
    }

    public void setAudiofocusPeer() {
        MprisAccessPointRpcService.doAction(getContext(),
                Constants.MPRIS_AP_ACTION_SET_AF_PEER, getApUrl(), getPort());
    }

    public void clearAudiofocusPeer() {
        MprisAccessPointRpcService.doAction(getContext(),
                Constants.MPRIS_AP_ACTION_CLEAR_AF_PEER, getApUrl(), getPort());
    }

    public void onPlaybackStatusPlaying() {
        MprisAccessPointRpcService.doAction(getContext(),
                Constants.MPRIS_AP_ACTION_PBSTATUS_PLAYING, getApUrl(), getPort());
    }

    public void onPlaybackStatusPaused() {
        MprisAccessPointRpcService.doAction(getContext(),
                Constants.MPRIS_AP_ACTION_PBSTATUS_PAUSED, getApUrl(), getPort());
    }

    @Override
    public void stop() {
        ignore();
        super.stop();
    }

    @Override
    public void start() {
        super.start();
        watch();
        init();
    }
}
