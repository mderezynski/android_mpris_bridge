package org.mpris.bridge.android.impl;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Base64InputStream;
import android.util.Log;

import org.mpris.bridge.android.interfaces.MprisServerBackchannelMarshaller;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by milosz on 02.10.14.
 */
public abstract class MprisFilteringServerDispatcher implements MprisServerBackchannelMarshaller {

    private final String server;

    public MprisFilteringServerDispatcher(final String server) {
        this.server = server;
    }

    @Override
    public void onTracklist(String server, ArrayList<String> tracks) {
        if(server.equals(this.server)) {
            gotTracklist(tracks);
        }
    }

    @Override
    public void onBackchannelInitialized(String server) {
        if(server.equals(this.server)) {
            gotBackchannelInitialized();
        }
    }

    @Override
    public final void onMetadata(String server, String title, String artist, String coverArtBase64) {

        Log.d("MprisFilteringServerDispatcher", "GOT METADATA: " + title + " " + artist);

        if(server.equals(this.server)) {

            Bitmap coverArt = null;

            if(coverArtBase64!=null&&!coverArtBase64.isEmpty()) {
                InputStream stream = null;
                try {
                    stream = new ByteArrayInputStream(coverArtBase64.getBytes("utf-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                Base64InputStream is = new Base64InputStream(stream, Base64.DEFAULT);
                coverArt = BitmapFactory.decodeStream(is);
            }

            gotMetadata(title, artist, coverArt);
        }
    }

    @Override
    public final void onPlaybackStatus(String server, String playbackStatus) {
        if(server.equals(this.server)) {
            gotPlaybackStatus(playbackStatus);
        }
    }

    @Override
    public final void onIdentity(String server, String identity, String iconBase64) {
        if(server.equals(this.server)) {

            InputStream stream = null;
            try {
                stream = new ByteArrayInputStream(iconBase64.getBytes("utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            Base64InputStream is = new Base64InputStream(stream, Base64.DEFAULT);

            final Bitmap icon = BitmapFactory.decodeStream(is);

            gotIdentity(identity, icon);
        }
    }
}
