package org.mpris.bridge.android.impl;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.mpris.bridge.android.Constants;

import org.mpris.bridge.android.interfaces.MprisServerInterface;
import org.mpris.bridge.android.interfaces.MprisTrackListInterface;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by milosz on 28.09.14.
 */
public class MprisServerRpcService extends IntentService {

    private MprisServerInterface proxyPlayer;
    private MprisTrackListInterface proxyTracklist;

    public MprisServerRpcService() {
        super("MprisServiceRpcService");
    }

    public static void doAction(final Context ctx, final String action, final String apUrl, final String server) {

        Intent intent = new Intent(ctx, MprisServerRpcService.class);
        intent.setAction(action);
        intent.putExtra(Constants.MPRIS_DATA_SERVICE, server);
        intent.putExtra(Constants.MPRIS_DATA_MPRISAP_URL, apUrl);
        ctx.startService(intent);

    }

    public static void doAction(final Context ctx, final String action, final String apUrl, final String server, int arg) {

        Intent intent = new Intent(ctx, MprisServerRpcService.class);
        intent.setAction(action);
        intent.putExtra(Constants.MPRIS_DATA_SERVICE, server);
        intent.putExtra(Constants.MPRIS_DATA_MPRISAP_URL, apUrl);
        intent.putExtra("track",arg);
        ctx.startService(intent);

    }

    public static void doAction(final Context ctx, final String action, final String apUrl, final String server, double arg) {

        Intent intent = new Intent(ctx, MprisServerRpcService.class);
        intent.setAction(action);
        intent.putExtra(Constants.MPRIS_DATA_SERVICE, server);
        intent.putExtra(Constants.MPRIS_DATA_MPRISAP_URL, apUrl);
        intent.putExtra("volume",arg);
        ctx.startService(intent);

    }

    public static class TrackList implements Serializable, Parcelable {

        private static final long serialVersionUID = 4861597073026532544L;

        public static final Parcelable.Creator<TrackList> CREATOR = new Parcelable.Creator<TrackList>() {
            public TrackList createFromParcel(Parcel in) {
                return new TrackList(in);
            }
            public TrackList[] newArray(int size) {
                return new TrackList[size];
            }
        };

        protected TrackList(Parcel in) {

            final int nTracks = in.readInt();

            tracks = new ArrayList<Map<String, String>>(nTracks);

            for( int n = 0 ; n < nTracks; n++) {
                Log.d("TrackList", "Reading track " + n);
                final int nEntries = in.readInt();
                final Map<String, String> track = new HashMap<String, String>(nEntries);
                for(int k = 0 ; k < nEntries; k++ ) {
                    track.put(in.readString(), in.readString());
                }
                tracks.add(track);
            }

        }

        protected List<Map<String, String>> tracks;

        public TrackList() {
            tracks = new ArrayList<Map<String, String>>();
        }

        public TrackList(List<Map<String, String>> tracks) {
            this.tracks = tracks;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {

            Log.d("TrackList", "Writing parcel");

            parcel.writeInt(tracks.size());

            for(Map<String, String> track : tracks ) {

                parcel.writeInt(track.size());

                for(String s : track.keySet()) {
                    parcel.writeString(s);
                    parcel.writeString(track.get(s));
                }
            }

        }

        public List<Map<String,String>> getTracks() {
            return tracks;
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if(intent==null||intent.getAction()==null)
            throw new IllegalArgumentException();

        final String action = intent.getAction();
        final String server = intent.
                getStringExtra(Constants.MPRIS_DATA_SERVICE);

        final RpcServerIfaceResolver resolver = new RpcServerIfaceResolver(server);

        try {
            final String uri = intent.getStringExtra(Constants.MPRIS_DATA_MPRISAP_URL);
            proxyPlayer = JsonRpcFactory.newServiceProxy(uri,
                    resolver.getIface(RpcServerIfaceResolver.IFACE_PLAYER));
            proxyTracklist = JsonRpcFactory.newTrackListProxy(uri,
                    resolver.getIface(RpcServerIfaceResolver.IFACE_TRACKLIST));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if(action.equals(Constants.MPRIS_SERVICE_ACTION_PLAYPAUSE)) {
            try {
                proxyPlayer.playPause();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        } else if(action.equals(Constants.MPRIS_SERVICE_ACTION_NEXT)) {
            try {
                proxyPlayer.next();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        } else if(action.equals(Constants.MPRIS_SERVICE_ACTION_PREVIOUS)) {
            try {
                proxyPlayer.previous();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        } else if(action.equals(Constants.MPRIS_SERVICE_ACTION_VOLUME)) {
            try {
                proxyPlayer.volume(intent.getDoubleExtra("volume", 50));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        } else if(action.equals(Constants.MPRIS_SERVICE_ACTION_TRACKLIST)) {
            try {
                proxyTracklist.getTrackList();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }  else if(action.equals(Constants.MPRIS_SERVICE_ACTION_GO_TO)) {
            try {
                proxyTracklist.goToTrack(intent.getIntExtra("track", 0));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
    }
}
