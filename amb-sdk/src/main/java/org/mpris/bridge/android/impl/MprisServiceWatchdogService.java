package org.mpris.bridge.android.impl;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;

import org.mpris.bridge.android.Constants;

import java.util.Random;

/**
 * Created by milosz on 22.10.14.
 */
public class MprisServiceWatchdogService extends Service {

    private final int MAX_RESTARTS = 2;
    private final long WATCH_PATIENCE = 5000; // 5 seconds

    private boolean watch = false;
    private int restartCount = 0;
    private long timestampDiscoveryStarted = 0;
    private Handler handler;
    private int serial = 0;
    private int targetSerial = 0;

    public static void start(final Context ctx) {
        Intent startIntent = new Intent(ctx, MprisServiceWatchdogService.class);
        ctx.startService(startIntent);
    }

    private BroadcastReceiver receiver1 = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if(targetSerial!=serial) {
                return;
            }

            watch = true;
            timestampDiscoveryStarted = SystemClock.uptimeMillis();

            serial = new Random().nextInt();
            targetSerial = serial;

            handler.postAtTime(new Runnable() {
                @Override
                public void run() {
                    if(watch&&restartCount<MAX_RESTARTS) {
                        Intent intentAf = new Intent(getApplicationContext(), MprisAudioFocusService.class);
                        stopService(intentAf);
                        startService(intentAf);
                        MprisBonjourService.startDiscovery(getApplicationContext());
                        targetSerial = new Random().nextInt();
                        restartCount++;
                    }
                }
            }, timestampDiscoveryStarted+WATCH_PATIENCE);
        }
    };

    private BroadcastReceiver receiver2 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            watch = false;
            targetSerial = 0;
            serial = 0;
            restartCount = 0;
        }
    };

    private BroadcastReceiver receiver3 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            watch = false;
            targetSerial = 0;
            serial = 1;
            restartCount = 0;
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        handler = new Handler(Looper.myLooper());

        final LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getApplicationContext());

        lbm.registerReceiver(receiver1, new IntentFilter(Constants.MPRIS_DISCOVER_ACTION_NOTIFY_STARTED));
        lbm.registerReceiver(receiver2, new IntentFilter(Constants.MPRIS_DISCOVER_ACTION_NOTIFY_STOPPED));
        lbm.registerReceiver(receiver3, new IntentFilter(Constants.MPRIS_AF_SERVICE_NOTIFY_CONNECTED));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        final LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getApplicationContext());

        lbm.unregisterReceiver(receiver1);
        lbm.unregisterReceiver(receiver2);
        lbm.unregisterReceiver(receiver3);
    }
}
