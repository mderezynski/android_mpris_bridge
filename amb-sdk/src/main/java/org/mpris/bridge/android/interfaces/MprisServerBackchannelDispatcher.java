package org.mpris.bridge.android.interfaces;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by milosz on 03.10.14.
 */
public interface MprisServerBackchannelDispatcher {

    public abstract void gotBackchannelInitialized();
    public abstract void gotMetadata(String title, String artist, Bitmap coverArt);
    public abstract void gotIdentity(String identity, Bitmap icon);
    public abstract void gotPlaybackStatus(String playbackStatus);
    public abstract void gotTracklist(ArrayList<String> tracks);

}
