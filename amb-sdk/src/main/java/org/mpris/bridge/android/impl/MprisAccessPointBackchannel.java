package org.mpris.bridge.android.impl;

import android.os.Looper;
import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.rpc.client.JsonRpcInvoker;
import org.json.rpc.commons.JsonErrorListener;
import org.json.rpc.server.JsonRpcExecutor;
import org.json.rpc.server.JsonRpcServerTransport;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;

/**
* Created by milosz on 01.10.14.
*/
public class MprisAccessPointBackchannel extends NanoHTTPD {

    public MprisAccessPointBackchannel(final List<RpcBackchannelInfo> infos,
                                       final JsonErrorListener errorListener) throws
            IOException {
        super(null, 0);
        this.errorListener = errorListener;
        executor = bind(infos);
    }

    private JsonRpcExecutor executor;
    private JsonErrorListener errorListener;

    private JsonRpcExecutor bind(final List<RpcBackchannelInfo> infos) {

        JsonRpcExecutor executor = new JsonRpcExecutor(errorListener);

        for(RpcBackchannelInfo info : infos ) {

            Log.d("MprisMultiplexingBackchannel","Adding "+
                info.ifaceRemoteName + " with "+info.marshaller+" for "+info.iface);

            executor.addHandler(info.ifaceRemoteName, info.marshaller, info.iface);
        }

        executor.setTarget(Looper.getMainLooper());

        return executor;
    }

    @Override
    public Response serve(final IHTTPSession session) throws JsonProcessingException {

        long cl = Integer.valueOf(session.getHeaders().get("content-length"));

        Log.d("MprisBackchannelServer","content-length:"+cl);

        final Map<String, String> files = new HashMap<String, String>();
        try {
            session.parseBody(files);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ResponseException e) {
            e.printStackTrace();
        }

        Log.d("MprisBackchannelServer","json: "+files.get("postData"));

        try {
            executor.execute(new JsonRpcServerTransport() {

                @Override
                public String readRequest() throws Exception {
                    return files.get("postData");
                }

                @Override
                public void writeResponse(String responseData) throws Exception {
                }

                @Override
                public void provideRequest(JsonRpcInvoker.JsonRequest req) {
                    request = req;
                }
            });
        } catch( Exception e ) {
            Log.e("MprisBackchannelServer","error",e);
        }

        JsonRpcInvoker.JsonResponse resp = new JsonRpcInvoker.JsonResponse();
        resp.jsonrpc = "2.0";
        resp.id = request.id;

        ObjectMapper mapper = JsonRpcInvoker.getNewObjectMapper();
        String response = mapper.writeValueAsString(resp);

        Response res = new Response(Response.Status.OK,"application/json",response);
        return res;
    }

    private JsonRpcInvoker.JsonRequest request;
}
