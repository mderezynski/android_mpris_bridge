package org.mpris.bridge.android.interfaces;

import org.json.rpc.commons.JsonRpcClientException;

import java.util.ArrayList;

/**
 * Created by milosz on 29.09.14.
 */
public interface MprisAccessPointInterface {

    public void discover(String origin) throws JsonRpcClientException;
    public void watch(String origin) throws JsonRpcClientException;
    public void ignore(String origin) throws JsonRpcClientException;
    public void addMonitor(String server, String origin) throws JsonRpcClientException;
    public void removeMonitor(String server, String origin) throws JsonRpcClientException;
    public void onPlaybackStatus(String origin, String playbackStatus) throws JsonRpcClientException;
    public void setAfPeer(String origin) throws JsonRpcClientException;
    public void clearAfPeer(String origin) throws JsonRpcClientException;

}
