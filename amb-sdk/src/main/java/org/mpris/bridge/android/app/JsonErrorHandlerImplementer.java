package org.mpris.bridge.android.app;


import org.json.rpc.commons.JsonErrorListener;

public interface JsonErrorHandlerImplementer {

    public JsonErrorListener getErrorListener();

}
