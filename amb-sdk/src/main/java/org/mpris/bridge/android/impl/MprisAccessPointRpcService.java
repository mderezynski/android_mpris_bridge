package org.mpris.bridge.android.impl;

import android.accounts.NetworkErrorException;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;

import org.json.rpc.commons.JsonRpcClientException;
import org.json.rpc.commons.JsonRpcRemoteException;
import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.interfaces.MprisAccessPointInterface;
import org.mpris.bridge.android.network.Utilities;

import java.net.MalformedURLException;
import java.net.SocketException;

/**
 * Created by milosz on 28.09.14.
 */
public class MprisAccessPointRpcService extends IntentService {

    private MprisAccessPointInterface proxy;

    public MprisAccessPointRpcService() {
        super("MprisAccessPointRpcService");
    }

    public static void doAction(final Context ctx, final String action, final String apUrl, final int port) {

        try {
            Intent intent = new Intent(ctx, MprisAccessPointRpcService.class);
            intent.setAction(action);
            intent.putExtra(Constants.MPRIS_DATA_MPRISAP_URL, apUrl);
            intent.putExtra(Constants.MPRIS_DATA_PORT, port);
            ctx.startService(intent);
        } catch( NullPointerException e ) {
            return;
        }
    }

    public static void doAction(final Context ctx, final String action, final String apUrl, final String server, final int port) {

        try {
            Intent intent = new Intent(ctx, MprisAccessPointRpcService.class);
            intent.setAction(action);
            intent.putExtra(Constants.MPRIS_DATA_SERVICE, server);
            intent.putExtra(Constants.MPRIS_DATA_MPRISAP_URL, apUrl);
            intent.putExtra(Constants.MPRIS_DATA_PORT, port);
            ctx.startService(intent);
        } catch( NullPointerException e ) {
            return;
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if(intent==null||intent.getAction()==null||!intent.hasExtra(Constants.MPRIS_DATA_MPRISAP_URL))
            throw new IllegalArgumentException();

        WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        if(wifi.getWifiState()!=WifiManager.WIFI_STATE_ENABLED||wifi.getConnectionInfo()==null||wifi.getConnectionInfo().getSSID()==null)
            return;

        try {
            final String uri = intent.getStringExtra(Constants.MPRIS_DATA_MPRISAP_URL);
            proxy = JsonRpcFactory.newAccessPointProxy(uri);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return;
        }

        final String action = intent.getAction();
        final int port = intent.getIntExtra(Constants.MPRIS_DATA_PORT, 0);

        if(action.equals(Constants.MPRIS_AP_ACTION_DISCOVER)) {
            try {
                proxy.discover(Utilities.getOriginUrl(getApplicationContext(), port));
            } catch( JsonRpcClientException e ) {
                e.printStackTrace();
            } catch( NetworkErrorException e ) {
                e.printStackTrace();
            } catch( JsonRpcRemoteException e ) {
                e.printStackTrace();
            } catch (SocketException e) {
                e.printStackTrace();
            }
        }
        else if(action.equals(Constants.MPRIS_AP_ACTION_MONITOR_SERVER)) {
            try {
                proxy.addMonitor(intent.getStringExtra(Constants.MPRIS_DATA_SERVICE), Utilities.getOriginUrl(getApplicationContext(), port));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (NetworkErrorException e) {
                e.printStackTrace();
            } catch( JsonRpcRemoteException e ) {
                e.printStackTrace();
            } catch (SocketException e) {
                e.printStackTrace();
            } catch( JsonRpcClientException e ) {
                e.printStackTrace();
            }
        }
        else if(action.equals(Constants.MPRIS_AP_ACTION_REMOVE_MONITOR)) {
            try {
                proxy.removeMonitor(intent.getStringExtra(Constants.MPRIS_DATA_SERVICE), Utilities.getOriginUrl(getApplicationContext(), port));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (NetworkErrorException e) {
                e.printStackTrace();
            } catch( JsonRpcRemoteException e ) {
                e.printStackTrace();
            } catch (SocketException e) {
                e.printStackTrace();
            } catch( JsonRpcClientException e ) {
                e.printStackTrace();
            }
        }
        else if(action.equals(Constants.MPRIS_AP_ACTION_WATCH)) {
            try {
                proxy.watch(Utilities.getOriginUrl(getApplicationContext(), port));
            } catch( JsonRpcClientException e ) {
                e.printStackTrace();
            } catch (NetworkErrorException e) {
                e.printStackTrace();
            } catch( JsonRpcRemoteException e ) {
                e.printStackTrace();
            } catch (SocketException e) {
                e.printStackTrace();
            }
        }
        else if(action.equals(Constants.MPRIS_AP_ACTION_IGNORE)) {
            try {
                proxy.ignore(Utilities.getOriginUrl(getApplicationContext(), port));
            } catch( JsonRpcClientException e ) {
                e.printStackTrace();
            } catch (NetworkErrorException e) {
                e.printStackTrace();
            } catch( JsonRpcRemoteException e ) {
                e.printStackTrace();
            } catch (SocketException e) {
                e.printStackTrace();
            }
        } else if (action.equals(Constants.MPRIS_AP_ACTION_PBSTATUS_PLAYING)) {
            try {
                proxy.onPlaybackStatus(Utilities.getOriginUrl(getApplicationContext(), port), "Playing");
            } catch (JsonRpcClientException e) {
                e.printStackTrace();
            } catch (NetworkErrorException e) {
                e.printStackTrace();
            } catch( JsonRpcRemoteException e ) {
                e.printStackTrace();
            } catch (SocketException e) {
                e.printStackTrace();
            }
        } else if (action.equals(Constants.MPRIS_AP_ACTION_SET_AF_PEER)) {
            try {
                proxy.setAfPeer(Utilities.getOriginUrl(getApplicationContext(), port));
            } catch (JsonRpcClientException e) {
                e.printStackTrace();
            } catch (NetworkErrorException e) {
                e.printStackTrace();
            } catch( JsonRpcRemoteException e ) {
                e.printStackTrace();
            } catch (SocketException e) {
                e.printStackTrace();
            }
        } else if (action.equals(Constants.MPRIS_AP_ACTION_CLEAR_AF_PEER)) {
            try {
                proxy.clearAfPeer(Utilities.getOriginUrl(getApplicationContext(), port));
            } catch (JsonRpcClientException e) {
                e.printStackTrace();
            } catch (NetworkErrorException e) {
                e.printStackTrace();
            } catch( JsonRpcRemoteException e ) {
                e.printStackTrace();
            } catch (SocketException e) {
                e.printStackTrace();
            }
        } else if (action.equals(Constants.MPRIS_AP_ACTION_PBSTATUS_PAUSED)) {
            try {
                proxy.onPlaybackStatus(Utilities.getOriginUrl(getApplicationContext(), port), "Paused");
            } catch (JsonRpcClientException e) {
                e.printStackTrace();
            }catch( JsonRpcRemoteException e ) {
                e.printStackTrace();
            } catch (NetworkErrorException e) {
                e.printStackTrace();
            } catch (SocketException e) {
                e.printStackTrace();
            }
        }
    }
}
