package org.mpris.bridge.android.app;

import android.app.Activity;
import android.app.Service;

import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.impl.RpcBackchannelInfo;
import org.mpris.bridge.android.interfaces.MprisAccessPointBackchannelListener;
import org.mpris.bridge.android.proxy.MprisAccessPointProxy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by milosz on 03.10.14.
 */
public abstract class MprisBaseDiscoveryService extends Service implements JsonErrorHandlerImplementer {

    private MprisAccessPointProxy proxy;

    protected abstract MprisAccessPointBackchannelListener getAccessPointListener();

    public MprisBaseDiscoveryService() {
    }

    public final MprisAccessPointProxy getProxy() {
        return proxy;
    }

    public final boolean switchProxy(final String apUrl) {

        if (proxy != null) {
            proxy.stop();
            proxy.shutdown();
        }

        final RpcBackchannelInfo info =
                new RpcBackchannelInfo(getAccessPointListener(),
                        MprisAccessPointBackchannelListener.class,
                        Constants.MPRIS_RPC_AP_INTERFACE);

        final List<RpcBackchannelInfo> infos =
                new ArrayList<RpcBackchannelInfo>();

        infos.add(info);

        try {
            proxy = new MprisAccessPointProxy(this, infos, apUrl, getErrorListener());

        } catch (IOException e) {
            e.printStackTrace();
        }

        proxy.start();

        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (proxy != null) {
            proxy.stop();
            proxy.shutdown();
        }
    }
}
