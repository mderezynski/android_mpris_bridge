package org.mpris.bridge.android.impl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.impl.MprisNotificationService;

/**
 * Created by milosz on 12.10.14.
 */
public class ServiceTerminationBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intentIn) {
        if(intentIn.getAction().equals(Constants.MPRIS_ACTION_TERMINATE_NOTIFICATION_SERVICE)) {
            Intent intent = new Intent(context.getApplicationContext(), MprisNotificationService.class);
            context.stopService(intent);
        }
    }
}
