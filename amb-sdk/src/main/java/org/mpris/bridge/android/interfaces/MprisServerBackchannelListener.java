package org.mpris.bridge.android.interfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
* Created by milosz on 01.10.14.
*/
public interface MprisServerBackchannelListener {

    public void onBackchannelInitialized(String server);
    public void onMetadata(String server, String title, String artist, String coverArtBase64);
    public void onIdentity(String server, String identity, String iconBase64);
    public void onPlaybackStatus(String server, String playbackStatus);
    public void onTracklist(String server, ArrayList<String> tracks);

}
