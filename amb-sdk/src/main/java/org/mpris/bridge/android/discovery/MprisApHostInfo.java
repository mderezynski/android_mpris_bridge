package org.mpris.bridge.android.discovery;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

/**
* Created by milosz on 12.10.14.
*/
public class MprisApHostInfo implements Parcelable {

    public String hostName;
    public String address;
    public Uri uri;
    public String wifiSSID;

    public MprisApHostInfo() {
    }

    @Override
    public int hashCode() {
        return Uri.parse("mprisap://"+hostName+"@"+wifiSSID).toString().hashCode();
    }

    public static final Parcelable.Creator CREATOR =
            new Parcelable.Creator() {
                public MprisApHostInfo createFromParcel(Parcel in) {
                    return new MprisApHostInfo(in);
                }

                public MprisApHostInfo[] newArray(int size) {
                    return new MprisApHostInfo[size];
                }
            };

    protected MprisApHostInfo(Parcel in) {
        hostName = in.readString();
        address = in.readString();
        uri = in.readParcelable(Uri.class.getClassLoader());
        wifiSSID = in.readString();
    }

    @Override
    public boolean equals(final Object other) {
        if(!(other instanceof MprisApHostInfo))
            return false;

        MprisApHostInfo h2 = (MprisApHostInfo) other;

        if(uri==null||h2.uri==null||wifiSSID==null||h2.wifiSSID==null)
            return false;

        if(uri.equals(h2.uri) && wifiSSID.equals(h2.wifiSSID))
            return true;

        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(hostName);
        parcel.writeString(address);
        parcel.writeParcelable(uri, 0);
        parcel.writeString(wifiSSID);
    }
}
