package org.mpris.bridge.android.impl;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.discovery.MprisApHostInfo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by milosz on 03.10.14.
 */
public class MprisBonjourService extends Service {

    private Set<MprisApHostInfo> cachedHosts = new HashSet<MprisApHostInfo>();

    public static void start(final Context ctx) {

        Intent intent = new Intent(ctx, MprisBonjourService.class);
        ctx.startService(intent);

    }

    public static void stop(final Context ctx) {

        Intent intent = new Intent(ctx, MprisBonjourService.class);
        ctx.stopService(intent);

    }

    public static void startDiscovery(final Context ctx) {

        Intent intent = new Intent(ctx, MprisBonjourService.class);
        intent.setAction(Constants.MPRIS_DISCOVER_ACTION_START);
        ctx.startService(intent);

    }

    public static void clearCache(final Context ctx) {

        Intent intent = new Intent(ctx, MprisBonjourService.class);
        intent.setAction(Constants.MPRIS_DISCOVER_ACTION_CLEAR_CACHE);
        ctx.startService(intent);

    }

    private WifiManager getWifiManager() {
        return (WifiManager) getSystemService(Context.WIFI_SERVICE);
    }

    private MprisNsdDiscoverListener nsdl = new MprisNsdDiscoverListener();

    private void notifyApDiscovered(final MprisApHostInfo info) {

        Intent intent = new Intent(Constants.MPRIS_SERVICE_RESULT_AP_DISCOVERED);
        intent.putExtra(Constants.MPRIS_DATA_AP_HOST_INFO, info);

        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcastSync(intent);
    }

    private void notifyApDisappeared(final MprisApHostInfo info) {

        Intent intent = new Intent(Constants.MPRIS_SERVICE_RESULT_AP_DISAPPEARED);
        intent.putExtra(Constants.MPRIS_DATA_AP_HOST_INFO, info);

        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcastSync(intent);
    }

    private class MprisNsdDiscoverListener implements NsdManager.DiscoveryListener {

        protected boolean active = false;

        public boolean isActive() {
            return active;
        }

        public void setActive(boolean active) {
            this.active = active;
        }

        //  Called as soon as service discovery begins.
        @Override
        public void onDiscoveryStarted(String regType) {
        }

        @Override
        public void onServiceFound(NsdServiceInfo service) {

            Log.d("MprisBonjourService","Service: "+service.getServiceType());

            if (service.getServiceType().equals("_mprisap._tcp.")) {

                NsdManager nsdm = (NsdManager) getApplicationContext().getSystemService(Context.NSD_SERVICE);

                nsdm.resolveService(service, new NsdManager.ResolveListener() {
                    @Override
                    public void onResolveFailed(NsdServiceInfo nsdServiceInfo, int i) {

                    }

                    @Override
                    public void onServiceResolved(NsdServiceInfo service) {
                        final MprisApHostInfo info = new MprisApHostInfo();

                        info.address = service.getHost().getHostAddress();
                        info.hostName = service.getServiceName();
                        info.uri = Uri.parse("http://"+service.getHost().getHostName()+":"+service.getPort());
                        info.wifiSSID = getWifiManager().getConnectionInfo().getSSID();

                        cachedHosts.add(info);

                        notifyApDiscovered(info);
                    }
                });
            }
        }

        @Override
        public void onServiceLost(NsdServiceInfo service) {

            final MprisApHostInfo info = new MprisApHostInfo();

            info.hostName = service.getServiceName();
            info.wifiSSID = getWifiManager().getConnectionInfo().getSSID();

            cachedHosts.remove(info);

            notifyApDisappeared(info);
        }

        @Override
        public void onDiscoveryStopped(String serviceType) {
        }

        @Override
        public void onStartDiscoveryFailed(String serviceType, int errorCode) {
        }

        @Override
        public void onStopDiscoveryFailed(String serviceType, int errorCode) {
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final int startFlags = START_STICKY;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(intent==null||intent.getAction()==null)
            return startFlags;

        final String action = intent.getAction();

        if(action.equals(Constants.MPRIS_DISCOVER_ACTION_STOP)) {

            if(nsdl.isActive())
            try {
                NsdManager nsdm = (NsdManager) getApplicationContext().getSystemService(Context.NSD_SERVICE);
                nsdm.stopServiceDiscovery(nsdl);
                nsdl.setActive(false);
            } catch( IllegalArgumentException e ) {

            }

        } else if (action.equals(Constants.MPRIS_DISCOVER_ACTION_START)) {

            Intent intentDiscoverStarted = new Intent(Constants.MPRIS_DISCOVER_ACTION_NOTIFY_STARTED);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcastSync(intentDiscoverStarted);

            if(nsdl.isActive()) {
                for(MprisApHostInfo info:cachedHosts) {
                    notifyApDiscovered(info);
                }
            } else {
                NsdManager nsdm = (NsdManager) getApplicationContext().getSystemService(Context.NSD_SERVICE);
                nsdm.discoverServices("_mprisap._tcp", NsdManager.PROTOCOL_DNS_SD, nsdl);
                nsdl.setActive(true);
            }
        } else if (action.equals(Constants.MPRIS_DISCOVER_ACTION_CLEAR_CACHE)) {
            for(MprisApHostInfo info:cachedHosts) {
                notifyApDisappeared(info);
            }
            cachedHosts.clear();
            if(nsdl.isActive())
                try {
                    NsdManager nsdm = (NsdManager) getApplicationContext().getSystemService(Context.NSD_SERVICE);
                    nsdm.stopServiceDiscovery(nsdl);
                    nsdl.setActive(false);
                } catch( IllegalArgumentException e ) {

                }
            Intent intentDiscoverStopped = new Intent(Constants.MPRIS_DISCOVER_ACTION_NOTIFY_STOPPED);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcastSync(intentDiscoverStopped);
        }

        return startFlags;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(nsdl.isActive())
            try {
                NsdManager nsdm = (NsdManager) getApplicationContext().getSystemService(Context.NSD_SERVICE);
                nsdm.stopServiceDiscovery(nsdl);
                nsdl.setActive(false);
            } catch( IllegalArgumentException e ) {

            }
    }

}
