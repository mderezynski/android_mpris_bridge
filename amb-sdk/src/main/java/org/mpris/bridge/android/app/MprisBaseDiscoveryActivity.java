package org.mpris.bridge.android.app;

import android.app.Activity;
import android.content.res.Configuration;

import org.json.rpc.commons.JsonErrorListener;
import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.impl.RpcBackchannelInfo;
import org.mpris.bridge.android.proxy.MprisAbsProxy;
import org.mpris.bridge.android.proxy.MprisAccessPointProxy;
import org.mpris.bridge.android.interfaces.MprisAccessPointBackchannelListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by milosz on 03.10.14.
 */
public abstract class MprisBaseDiscoveryActivity extends Activity implements JsonErrorHandlerImplementer {

    private MprisAccessPointProxy proxy;
    protected abstract MprisAccessPointBackchannelListener getAccessPointListener();

    public MprisBaseDiscoveryActivity() {
    }

    protected void stopProxy() {
        if (proxy != null) {
            proxy.stop();
            proxy.shutdown();
            proxy = null;
        }
    }

    public final MprisAccessPointProxy getProxy() {
        return proxy;
    }

    public final boolean switchProxy(final String apUrl) {

        if(proxy!=null&&proxy.getApUrl().equals(apUrl)) {
            return false;
        }

        stopProxy();

        final RpcBackchannelInfo info =
                new RpcBackchannelInfo(getAccessPointListener(),
                        MprisAccessPointBackchannelListener.class,
                        Constants.MPRIS_RPC_AP_INTERFACE);

        final List<RpcBackchannelInfo> infos =
                new ArrayList<RpcBackchannelInfo>();

        infos.add(info);

        try {
            proxy = new MprisAccessPointProxy(getApplicationContext(), infos, apUrl, getErrorListener());
        } catch (IOException e) {
            e.printStackTrace();
        }

        proxy.start();

        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopProxy();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopProxy();
    }
}
