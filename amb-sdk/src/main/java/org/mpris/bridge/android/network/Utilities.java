package org.mpris.bridge.android.network;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;

import java.net.SocketException;

/**
 * Created by milosz on 10.10.14.
 */
public class Utilities {
    public static String getOriginUrl(final Context ctx, int port) throws NetworkErrorException, SocketException {

        WifiManager wifiMgr = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);

        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

        if (wifiInfo != null) {
            return "http://"+ Formatter.formatIpAddress(wifiInfo.getIpAddress())+":"+port;
        }

        throw new NetworkErrorException();
    }
}
