package org.mpris.bridge.android.interfaces;

import java.util.List;
import java.util.Map;

/**
 * Created by milosz on 29.09.14.
 */
public interface MprisTrackListInterface {

    public void getTrackList();
    public void goToTrack(int track);
}
