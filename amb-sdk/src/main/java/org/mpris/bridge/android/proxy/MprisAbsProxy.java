package org.mpris.bridge.android.proxy;

import android.content.Context;


import org.json.rpc.commons.JsonErrorListener;
import org.mpris.bridge.android.discovery.MprisApHostInfo;
import org.mpris.bridge.android.impl.MprisAccessPointBackchannel;
import org.mpris.bridge.android.impl.RpcBackchannelInfo;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * Created by milosz on 29.09.14.
 */
public abstract class MprisAbsProxy {

    protected Context ctx;
    private MprisAccessPointBackchannel server;
    private String apUrl;

    public MprisAbsProxy(final Context ctx, List<RpcBackchannelInfo> infos,
                         final String apUrl, final JsonErrorListener l) throws IOException {
        this.ctx = ctx;
        this.apUrl = apUrl;

        if(infos!=null) {
            this.server = new MprisAccessPointBackchannel(infos, l);
        }
    }

    public final Context getContext() {
        return ctx;
    }

    public final String getApUrl() {
        return apUrl;
    }

    public final int getPort() { return server.getListeningPort(); }

    public final void shutdown() {
        ctx = null;
    }

    public void start() {
        if(server!=null)
        try {
            server.start();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        if(server!=null) {
            try {
                server.stop();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }
        }
    }
}
