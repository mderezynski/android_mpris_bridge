package org.mpris.bridge.android.interfaces;

/**
* Created by milosz on 01.10.14.
*/
public interface MprisAudioFocusBackchannelListener {

    public void onServerPlaying(String server);
    public void onRequestAudiofocus(String server);
    public void onAbandonAudiofocus(String server);

}
