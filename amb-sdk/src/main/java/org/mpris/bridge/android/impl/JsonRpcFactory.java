package org.mpris.bridge.android.impl;

import android.net.Uri;

import org.json.rpc.client.HttpJsonRpcClientTransport;
import org.json.rpc.client.JsonRpcInvoker;
import org.mpris.bridge.android.interfaces.MprisAccessPointInterface;
import org.mpris.bridge.android.interfaces.MprisServerInterface;
import org.mpris.bridge.android.interfaces.MprisTrackListInterface;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by milosz on 02.10.14.
 */
public class JsonRpcFactory {

    private final static String REMOTE_IFACE_MPRIS_AP = "mprisap";

    public static MprisAccessPointInterface newAccessPointProxy(final String uri) throws MalformedURLException {

        HttpJsonRpcClientTransport transport;
        transport = new HttpJsonRpcClientTransport(Uri.parse(uri));
        JsonRpcInvoker invoker = new JsonRpcInvoker();
        return invoker.get(transport, REMOTE_IFACE_MPRIS_AP, MprisAccessPointInterface.class);

    }

    public static MprisServerInterface newServiceProxy(final String uri, final String iface) throws MalformedURLException {

        HttpJsonRpcClientTransport transport;
        transport = new HttpJsonRpcClientTransport(Uri.parse(uri));
        JsonRpcInvoker invoker = new JsonRpcInvoker();
        return invoker.get(transport, iface, MprisServerInterface.class);

    }

    public static MprisTrackListInterface newTrackListProxy(final String uri, final String iface) throws MalformedURLException {

        HttpJsonRpcClientTransport transport;
        transport = new HttpJsonRpcClientTransport(Uri.parse(uri));
        JsonRpcInvoker invoker = new JsonRpcInvoker();
        return invoker.get(transport, iface, MprisTrackListInterface.class);

    }

}
