package org.mpris.bridge.android.impl;

/**
 * Created by milosz on 05.10.14.
 */
public class RpcServerIfaceResolver {

    private String server;

    public final static String IFACE_PLAYER = "player";
    public final static String IFACE_TRACKLIST = "tracklist";

    public RpcServerIfaceResolver(final String name) {
        server = name.substring(23);
    }

    public final String getName() { return server; }

    public final String getIface(final String iface) {
        return "server."+server+"."+iface;
    }

}
