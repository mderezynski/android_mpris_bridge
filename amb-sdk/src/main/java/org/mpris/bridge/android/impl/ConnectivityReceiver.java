package org.mpris.bridge.android.impl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

/**
* Created by milosz on 13.10.14.
*/
public class ConnectivityReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
            NetworkInfo networkInfo =
                    intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            if(networkInfo.isConnected()) {
                MprisBonjourService.startDiscovery(context.getApplicationContext());
            } else if (!networkInfo.isConnected()) {
                MprisBonjourService.clearCache(context.getApplicationContext());
            }
        }
    }
}
