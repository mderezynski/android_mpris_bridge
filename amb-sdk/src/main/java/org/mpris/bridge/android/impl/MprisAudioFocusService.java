package org.mpris.bridge.android.impl;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.inject.internal.cglib.core.Local;

import org.json.rpc.commons.JsonErrorListener;
import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.R;
import org.mpris.bridge.android.discovery.MprisApHostInfo;
import org.mpris.bridge.android.interfaces.MprisAudioFocusBackchannelListener;
import org.mpris.bridge.android.proxy.MprisAccessPointProxy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by milosz on 06.10.14.
 */
public class MprisAudioFocusService extends Service {

    private AudioManager am;

    private MprisAccessPointProxy apProxy;
    private AudioFocusListener afListener;
    private MprisApHostInfo hostInfo;
    private Handler handler;
    private JsonErrorListener errorListener;
    private AmListener oldListener = null;
    private AmListener listener;

    private class AmListener implements AudioManager.OnAudioFocusChangeListener {

        private boolean ignore = false;

        public void setIgnore() {
            ignore = true;
        }

        @Override
        public void onAudioFocusChange(int focusChange) {

            if(ignore)
                return;

            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {

                Log.d("MprisAudioFocusService","lost audiofocus transiently");

                focusStatus = AudioManager.AUDIOFOCUS_LOSS;

                if(apProxy!=null) {
                    apProxy.onPlaybackStatusPlaying();
                }

            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {

                Log.d("MprisAudioFocusService","gained audiofocus");

                focusStatus = AudioManager.AUDIOFOCUS_GAIN;

                if(apProxy!=null) {
                    apProxy.onPlaybackStatusPaused();
                }

            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {

                Log.d("MprisAudioFocusService","lost audiofocus");

                focusStatus = AudioManager.AUDIOFOCUS_LOSS;

                if(apProxy!=null) {
                    apProxy.onPlaybackStatusPlaying();
                }

            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {

            }
        }
    }

    private final static String MPRIS_AF_SERVICE_TITLE = "MPRIS AudioFocus Bridge";

    private class InternalErrorListener implements JsonErrorListener {

        @Override
        public void onTransportError(String method, Object[] params) {

        }
    }

    private BroadcastReceiver receiver2 = new BroadcastReceiver() {

        @Override
        public void onReceive(
                Context context, Intent intent) {

            final MprisApHostInfo info = intent.getParcelableExtra(Constants.MPRIS_DATA_AP_HOST_INFO);

            WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

            final String hostName = getApplicationContext().getSharedPreferences("preferred-ap",
                    MODE_PRIVATE
                    ).getString("org.mpris.bridge.android.network" +
                    "."+wifiManager.getConnectionInfo().getSSID(), null);

            if(apProxy==null&&hostName!=null&&hostName.equals(info.hostName)) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent(Constants.MPRIS_AF_SERVICE_NOTIFY_CONNECTED));

                        hostInfo = info;
                        initProxy();
                    }
                });
            }
        }
    };

    private BroadcastReceiver receiver3 = new BroadcastReceiver() {

        @Override
        public void onReceive(
                Context context, Intent intent) {

            final MprisApHostInfo info = intent.getParcelableExtra(Constants.MPRIS_DATA_AP_HOST_INFO);

            handler.post(new Runnable() {
                @Override
                public void run() {
                    if(apProxy==null||info.hostName.equals(hostInfo.hostName)) {
                        disconnect();
                    }
                }
            });

        }
    };

    private BroadcastReceiver receiver4 = new BroadcastReceiver() {

        @Override
        public void onReceive(
                Context context, Intent intent) {

            handler.post(new Runnable() {
                @Override
                public void run() {
                    if(apProxy==null) {
                        searchMode();
                    }
                }
            });

        }
    };

    private BroadcastReceiver receiver5 = new BroadcastReceiver() {

        @Override
        public void onReceive(
                Context context, Intent intent) {

            handler.post(new Runnable() {
                @Override
                public void run() {
                    if(apProxy!=null) {
                        disconnect();
                    }
                }
            });

        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        handler = new Handler(Looper.myLooper());

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver2,
                new IntentFilter(Constants.MPRIS_SERVICE_RESULT_AP_DISCOVERED));

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver3,
                new IntentFilter(Constants.MPRIS_SERVICE_RESULT_AP_DISAPPEARED));

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver4,
                new IntentFilter(Constants.MPRIS_DISCOVER_ACTION_NOTIFY_STARTED));

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver5,
                new IntentFilter(Constants.MPRIS_DISCOVER_ACTION_NOTIFY_STOPPED));

        am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        listener = new AmListener();
        am.requestAudioFocus(listener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

        handler.postAtTime(new Runnable() {
            @Override
            public void run() {
                am.abandonAudioFocus(listener);
            }
        }, SystemClock.uptimeMillis() + 1000);
    }

    private String oldServer = "";

    private class AudioFocusListener implements MprisAudioFocusBackchannelListener {

        @Override
        public void onServerPlaying(String server) {
            if(!server.equals(oldServer)) {
                oldServer = server;
                Intent intent = new Intent(getApplicationContext(), MprisNotificationService.class);
                intent.setAction(Constants.MPRIS_BASE_CONTROL_SERVICE_ACTION_START);
                intent.putExtra(Constants.MPRIS_DATA_MPRISAP_URL, apProxy.getApUrl());
                intent.putExtra(Constants.MPRIS_DATA_SERVICE, server);
                startService(intent);
            }
        }

        @Override
        public void onRequestAudiofocus(String server) {

            Log.d("MprisAudioFocusService", "Requesting audiofocus on behalf of " + server);

                am.requestAudioFocus(listener,
                        AudioManager.STREAM_MUSIC,
                        AudioManager.AUDIOFOCUS_GAIN);

        }

        @Override
        public void onAbandonAudiofocus(String server) {
            //am.abandonAudioFocus(afChangeListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        final LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getApplicationContext());
        lbm.unregisterReceiver(receiver2);
        lbm.unregisterReceiver(receiver3);
        lbm.unregisterReceiver(receiver4);
        lbm.unregisterReceiver(receiver5);

        stopForeground(true);
        stopProxy();
    }

    private void shutdownProxy() {
        if(apProxy!=null) {
            focusStatus = AudioManager.AUDIOFOCUS_LOSS;
            apProxy.stop();
            apProxy.shutdown();
            apProxy = null;
            oldServer = "";
        }
    }

    private void stopProxy() {
        if(apProxy!=null) {
            focusStatus = AudioManager.AUDIOFOCUS_LOSS;
            apProxy.clearAudiofocusPeer();
            apProxy.stop();
            apProxy.shutdown();
            apProxy = null;
            oldServer = "";
        }
    }

    private int focusStatus;

    private void initProxy() {

        stopProxy();

        errorListener = new InternalErrorListener();
        afListener = new AudioFocusListener();

        final RpcBackchannelInfo info =
                new RpcBackchannelInfo(afListener,
                        MprisAudioFocusBackchannelListener.class,
                        Constants.MPRIS_RPC_AF_INTERFACE);

        final List<RpcBackchannelInfo> infos =
                new ArrayList<RpcBackchannelInfo>();

        infos.add(info);

        try {
            apProxy = new MprisAccessPointProxy(getApplicationContext(),infos,
                    hostInfo.uri.toString(), errorListener);
            apProxy.start();
            apProxy.setAudiofocusPeer();
            connected();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void searchMode() {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentTitle(MPRIS_AF_SERVICE_TITLE);
        builder.setContentText("Searching...");
        builder.setSmallIcon(android.R.drawable.presence_offline);
        builder.setProgress(100,0,true);
        startForeground(1, builder.build());

    }

    private void connected() {


        final MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.af_online);
        mp.start();

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentTitle(MPRIS_AF_SERVICE_TITLE);
        builder.setContentText(hostInfo.hostName + " on " + hostInfo.address);
        builder.setSmallIcon(android.R.drawable.presence_online);

        startForeground(1, builder.build());
    }

    private void disconnect() {
        shutdownProxy();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentTitle(MPRIS_AF_SERVICE_TITLE);
        builder.setContentText("Disconnected");
        builder.setSmallIcon(android.R.drawable.presence_offline);
        startForeground(1, builder.build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(intent!=null) {

            if(intent.getAction().equals(Constants.MPRIS_AF_SERVICE_CONNECT)) {

                final WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

                hostInfo = intent.getParcelableExtra(Constants.MPRIS_DATA_AP_HOST_INFO);

                final String hostName = getApplicationContext().getSharedPreferences("preferred-ap",
                        MODE_PRIVATE
                ).getString("org.mpris.bridge.android.network" +
                        "."+wifiManager.getConnectionInfo().getSSID(), null);

                if(hostName!=null&&hostName.equals(hostInfo.hostName)) {
                    return START_STICKY;
                }

                getApplicationContext().getSharedPreferences("preferred-ap",
                        MODE_PRIVATE
                ).edit().putString("org.mpris.bridge.android.network" +
                        "."+wifiManager.getConnectionInfo().getSSID(), hostInfo.hostName).commit();

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        initProxy();
                    }
                });

            }
        }

        return START_STICKY;
    }

}
