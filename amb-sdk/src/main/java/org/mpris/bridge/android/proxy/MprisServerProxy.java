package org.mpris.bridge.android.proxy;

import android.content.Context;

import org.json.rpc.commons.JsonErrorListener;
import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.impl.MprisAccessPointRpcService;
import org.mpris.bridge.android.impl.MprisServerRpcService;
import org.mpris.bridge.android.impl.RpcBackchannelInfo;

import java.io.IOException;
import java.util.List;

/**
 * Created by milosz on 29.09.14.
 */
public class MprisServerProxy extends MprisAbsProxy {

    private final String name;

    public MprisServerProxy(final Context ctx, final List<RpcBackchannelInfo> infos,
                            final String apUrl, final String name,
                            final JsonErrorListener l) throws IOException {

        super(ctx, infos, apUrl, l);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void raise() {
        MprisServerRpcService.doAction(ctx, Constants.MPRIS_SERVICE_ACTION_RAISE, getApUrl(), name);
    }

    public void playPause() {
        MprisServerRpcService.doAction(ctx, Constants.MPRIS_SERVICE_ACTION_PLAYPAUSE, getApUrl(), name);
    }

    public void next() {
        MprisServerRpcService.doAction(ctx, Constants.MPRIS_SERVICE_ACTION_NEXT, getApUrl(), name);
    }

    public void previous() {
        MprisServerRpcService.doAction(ctx, Constants.MPRIS_SERVICE_ACTION_PREVIOUS, getApUrl(), name);
    }

    public void goToTrack(int n) {
        MprisServerRpcService.doAction(ctx, Constants.MPRIS_SERVICE_ACTION_GO_TO, getApUrl(), name, n);
    }


    public void setVolume(double i) {
        MprisServerRpcService.doAction(ctx, Constants.MPRIS_SERVICE_ACTION_VOLUME, getApUrl(), name, i);
    }

    public void getTrackList() {
        MprisServerRpcService.doAction(ctx, Constants.MPRIS_SERVICE_ACTION_TRACKLIST, getApUrl(), name, getPort());
    }

    public void start() {
        super.start();
        MprisAccessPointRpcService.doAction(ctx, Constants.MPRIS_AP_ACTION_MONITOR_SERVER, getApUrl(), name, getPort());
    }

    public void stop() {
        super.stop();
        MprisAccessPointRpcService.doAction(ctx, Constants.MPRIS_AP_ACTION_REMOVE_MONITOR, getApUrl(), name, getPort());
    }

}
