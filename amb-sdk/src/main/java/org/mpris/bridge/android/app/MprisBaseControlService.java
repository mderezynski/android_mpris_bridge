package org.mpris.bridge.android.app;

import android.accounts.NetworkErrorException;
import android.app.Service;
import android.content.Intent;
import android.util.Log;

import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.impl.RpcBackchannelInfo;
import org.mpris.bridge.android.interfaces.MprisServerBackchannelListener;
import org.mpris.bridge.android.interfaces.MprisServerBackchannelMarshaller;
import org.mpris.bridge.android.network.Utilities;
import org.mpris.bridge.android.proxy.MprisServerProxy;

import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by milosz on 03.10.14.
 */
public abstract class MprisBaseControlService extends Service implements JsonErrorHandlerImplementer {

    private MprisServerProxy proxy;

    protected abstract MprisServerBackchannelMarshaller getListener(final String server);

    public MprisBaseControlService() {
    }

    public final MprisServerProxy getProxy() {
        return proxy;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(intent==null||intent.getAction()==null)
            return START_STICKY;

        if(intent.getAction().equals(Constants.MPRIS_BASE_CONTROL_SERVICE_ACTION_START)) {
            if(getProxy()!=null) {
                getProxy().stop();
                getProxy().shutdown();
            }
            proxy = null;
        }

        if(getProxy()==null)
            initializeProxy(intent);

        assert proxy != null;
        try {
            Log.d("MprisBaseControlService","origin is: "+ Utilities.getOriginUrl(getApplicationContext(), proxy.getPort()));
        } catch (NetworkErrorException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            e.printStackTrace();
        }

        if(intent.getAction().equals("pause")) {
            proxy.playPause();
        } else if(intent.getAction().equals("next")) {
            proxy.next();
        }


        return START_STICKY;
    }

    protected abstract void onProxyInitialized();

    protected void initializeProxy(final Intent intent) {

        final String apUrl = intent.getStringExtra(Constants.MPRIS_DATA_MPRISAP_URL);
        final String server = intent.getStringExtra(Constants.MPRIS_DATA_SERVICE);

        final RpcBackchannelInfo info =
          new RpcBackchannelInfo(getListener(server),
                  MprisServerBackchannelListener.class,
                  Constants.MPRIS_RPC_SERVER_INTERFACE);

        final List<RpcBackchannelInfo> infos =
          new ArrayList<RpcBackchannelInfo>();

        infos.add(info);

        try {
            proxy = new MprisServerProxy(getApplicationContext(), infos, apUrl, server, getErrorListener());
        } catch (IOException e) {
            e.printStackTrace();
        }

        proxy.start();
        onProxyInitialized();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(proxy!=null) {
            proxy.stop();
            proxy.shutdown();
        }
    }
}
