package org.mpris.bridge.android.impl;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;

import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.R;

public class MediaPlayerNotificationStyle {

	private final String artist, title;
	private final Context ctx;
	private final int density;
    private final Bitmap icon2;
    private final Bitmap icon1;
    private final String apUrl;
    private final String server;
    private final String pbStatus;

    public MediaPlayerNotificationStyle(final Context c, final String artist, final String title, final Bitmap icon1, final Bitmap icon2, final int d, final String server, final String apUrl, final String pbStatus) {
		ctx = c;
		density = d;
        this.artist = artist;
        this.title = title;
        this.icon1 = icon1;
        this.icon2 = icon2;
        this.server = server;
        this.apUrl = apUrl;
        this.pbStatus = pbStatus;
	}

    private Bitmap getIcon() {

        if(icon1==null)
            return null;

       	int w = 96 ;
       	
      	switch(density){
       	     case DisplayMetrics.DENSITY_LOW:
       	    	 
       	    	 		 w = 48 ;
       	                 break;
       	                 
       	     case DisplayMetrics.DENSITY_MEDIUM:
       	    	 
       	    	 		 w = 64 ;
       	                 break;
       	                 
       	     case DisplayMetrics.DENSITY_HIGH:
       	    	 
       	    	 		 w = 96 ;
       	                 break;
       	                 
       	     case DisplayMetrics.DENSITY_XHIGH:
       	     case DisplayMetrics.DENSITY_XXHIGH:
       	    	 
		    	 		 w = 192 ;
		                 break;
       	}
      	
        return Bitmap.createScaledBitmap(this.icon1, w, w, true);
    }

	public Notification build(NotificationCompat.Builder b) {

		b.setOnlyAlertOnce(true);

		final Intent intentFF = new Intent(ctx.getApplicationContext(),
				MprisNotificationService.class);
		intentFF.setAction("next");
        intentFF.putExtra(Constants.MPRIS_DATA_SERVICE, server);
        intentFF.putExtra(Constants.MPRIS_DATA_MPRISAP_URL, apUrl);
		final PendingIntent pendingIntentFF = PendingIntent.getService(
				ctx.getApplicationContext(), 2000, intentFF,
				PendingIntent.FLAG_UPDATE_CURRENT);

		b.addAction(android.R.drawable.ic_media_next,
				"Next",
				pendingIntentFF);

		final Intent intentPS = new Intent(ctx.getApplicationContext(),
				MprisNotificationService.class);
		intentPS.setAction("pause");
        intentPS.putExtra(Constants.MPRIS_DATA_SERVICE, server);
        intentPS.putExtra(Constants.MPRIS_DATA_MPRISAP_URL, apUrl);
		final PendingIntent pendingIntentPS = PendingIntent.getService(
				ctx.getApplicationContext(), 2001, intentPS,
				PendingIntent.FLAG_UPDATE_CURRENT);

        b.addAction((pbStatus.equals("Playing")) ? android.R.drawable.ic_media_pause : android.R.drawable.ic_media_play,
                (pbStatus.equals("Playing")) ? "Pause" : "Resume",
                pendingIntentPS);

        b.setSmallIcon(R.drawable.icon_control);

        if(icon1!=null)
           b.setLargeIcon(getIcon());

        if(title!=null)
           b.setContentTitle(title);
        else
           b.setContentTitle("");

        if(artist!=null)
           b.setContentText(artist);
        else
           b.setContentText("");

        b.setAutoCancel(true);

        final Intent intentStopService = new Intent(ctx.getApplicationContext(),
                ServiceTerminationBroadcastReceiver.class);

        intentStopService.setAction(Constants.MPRIS_ACTION_TERMINATE_NOTIFICATION_SERVICE);

        final PendingIntent pendingIntentTerminate = PendingIntent.getBroadcast(
                ctx.getApplicationContext(), 0, intentStopService,
                PendingIntent.FLAG_CANCEL_CURRENT);

        b.setDeleteIntent(pendingIntentTerminate);

		Notification n = b.build();

		n.flags |= Notification.FLAG_ONLY_ALERT_ONCE;
        n.flags |= Notification.FLAG_AUTO_CANCEL;

		return n;
	}
}
